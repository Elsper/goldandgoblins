﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class GoblinSkin : MonoBehaviour
{
    private const string ANIMATION_NAME_WORK = "WORK";
    private const string ANIMATION_NAME_LVLUP = "LVLUP";

    [SerializeField] private Animator _selfAnimator;

    [SerializeField] private GameObject[] _tools;
    [SerializeField] private Animator[] _toolsAnimator;

    private int _currentIndexByLevel = 0;
    private bool _stateWork;

    public void SetAnimationByLevel(int index)
    {
        _tools[_currentIndexByLevel].SetActive(false);
        _tools[index].SetActive(true);


        for(int i = _currentIndexByLevel; i < index; i++)
        {
            _selfAnimator.SetTrigger(ANIMATION_NAME_LVLUP);
        }

        //_toolsAnimator[index].enabled = false;
        //_toolsAnimator[index].enabled = true;
        _toolsAnimator[index].SetBool(ANIMATION_NAME_WORK, _stateWork);


        _currentIndexByLevel = index;
    }

    public void SetWorkState(bool state)
    {
        _stateWork = state;

        _selfAnimator.SetBool(ANIMATION_NAME_WORK, state);
        _toolsAnimator[_currentIndexByLevel].SetBool(ANIMATION_NAME_WORK, state);
    }

    public int LengthArrayLevelAnimation() => _tools.Length;
}