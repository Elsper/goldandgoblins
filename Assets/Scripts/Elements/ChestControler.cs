using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ChestControler : MonoBehaviour
{
    [SerializeField] private GameObject _chestLid;
    [SerializeField] private ChestParticlesController _chestParticles;

    public void OpenChest(float time = 0)
    {
        _chestLid.transform.DOKill();
        _chestLid.transform.DOLocalRotate(new Vector3(0, 40, -180), time);
    }
    public void CloseChest(float time = 0)
    {
        if (_chestLid == null) return;
        _chestLid.transform.DOKill();
        _chestLid.transform.DOLocalRotate(new Vector3(0, 0, -180), time);
    }

    public void ShowParticleIdle() => _chestParticles.StartIdle();
    public void ShowParticleOpening() => _chestParticles.StartOpening();
    public void OffAllParticles() => _chestParticles.OffParticles();

    private void OnDisable()
    {
        OffAllParticles();
    }
    private void OnEnable()
    {
        ShowParticleIdle();
    }
}
