﻿using UnityEngine;
using System.Collections;

public class DestroyableStone : MonoBehaviour
{
    [SerializeField] private DestroyableObject _destroyableObject;
    private TypeDestroyableObject _type;

    private Vector2Int _thisPosition;

    private void Start()
    {
        _destroyableObject.ActionWhenDurabilityIsOver +=IsDurationOver;

        _thisPosition = new Vector2Int((int)(transform.position.x), (int)(transform.position.z));
        _type = _destroyableObject.Type;
    }
    private void OnDestroy()
    {
        _destroyableObject.ActionWhenDurabilityIsOver -= IsDurationOver;
    }
    public void IsDurationOver()
    {
        int level = _destroyableObject.Level;


        switch (_type)
        {
            case TypeDestroyableObject.JustRock:
                EntryPoint.Instance.PicableResourcesManager.MakeGoldOnScene(level, _thisPosition);
                break;
            case TypeDestroyableObject.GreenStoneSmall:
                EntryPoint.Instance.PicableResourcesManager.MakeHardCurrencyOnScene(3, _thisPosition); //Checked
                break;
            case TypeDestroyableObject.GreenStoneLarge:
                EntryPoint.Instance.PicableResourcesManager.MakeHardCurrencyOnScene(10, _thisPosition);//Checked
                break;
            case TypeDestroyableObject.BlueStoneSmall:
                EntryPoint.Instance.PicableResourcesManager.MakeSoftCurrencyOnScene(30, _thisPosition);
                break;
            case TypeDestroyableObject.BlueStoneLarge:
                EntryPoint.Instance.PicableResourcesManager.MakeSoftCurrencyOnScene(100, _thisPosition);
                break;
            default:
                break;
        }
    }
}
