using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public enum TypeDestroyableObject
{
    JustRock = 1,
    GreenStoneSmall = 2,
    GreenStoneLarge = 3,
    Gate = 4,
    Exit = 5,
    Mine = 6,
    BlueStoneSmall = 7,
    BlueStoneLarge = 8
}

public class DestroyableObject : MonoBehaviour
{
    [SerializeField] private GameObject _targetForGoblins;

    [Header("Duration")]
    [SerializeField] public TypeDestroyableObject Type;
    [SerializeField] private int _level = 1;

    [Header("GameObjects Destroy when duration is over ")]
    [SerializeField] private List<GameObject> _listGOForDestroy;


    [SerializeField] GameObject _circleProgressBar;
    [SerializeField] TextMeshProUGUI _circleProgressBarText;
    private CircleAndText _circleAndText;


    [SerializeField] GameObject _lvlCircleBG;
    [SerializeField] TextMeshProUGUI _lvlText;

    private Vector2Int _thisPosition;

    private double _perSecConsumeDurability = 0f;
    private bool _alreadyStarted = false;

    private double _currentDurability;
    private double _maxDurability;

    private List<Goblin> _goblins = new List<Goblin>();
    public event Action ActionWhenDurabilityIsOver;

    public double PerSecConsumeDurability { get { return _perSecConsumeDurability; } set { _perSecConsumeDurability = value; } }
    public double CurrentDurability
    {
        get { return _currentDurability; }
        set
        {
            _currentDurability = value;
            if (_currentDurability <= 0)
                Destroy(this);

            if (_currentDurability < _maxDurability)
                ConsumeDurability(0.000001f);
        }
    }

    private void Awake()
    {
        _circleAndText = _circleProgressBar.GetComponent<CircleAndText>(); _thisPosition = new Vector2Int((int)(transform.position.x), (int)(transform.position.z));
        _circleAndText.gameObject.SetActive(true);

        _maxDurability = Balance.DurabilityForStone(_level, Type);
        if (!_alreadyStarted)
            _currentDurability = _maxDurability;

        _circleProgressBar.SetActive(false);

        switch (Type)
        {
            case TypeDestroyableObject.Gate:
                EntryPoint.Instance.LevelsManager.CurrentLevel.AddDestroyableObjectFromScene(this, new Vector2Int((int)transform.position.x - 2, (int)transform.position.z));
                EntryPoint.Instance.LevelsManager.CurrentLevel.AddDestroyableObjectFromScene(this, new Vector2Int((int)transform.position.x - 1, (int)transform.position.z));
                EntryPoint.Instance.LevelsManager.CurrentLevel.AddDestroyableObjectFromScene(this, new Vector2Int((int)transform.position.x - 0, (int)transform.position.z));
                EntryPoint.Instance.LevelsManager.CurrentLevel.AddDestroyableObjectFromScene(this, new Vector2Int((int)transform.position.x + 1, (int)transform.position.z));
                EntryPoint.Instance.LevelsManager.CurrentLevel.AddDestroyableObjectFromScene(this, new Vector2Int((int)transform.position.x + 2, (int)transform.position.z));
                break;
            case TypeDestroyableObject.Exit:
                EntryPoint.Instance.LevelsManager.CurrentLevel.AddDestroyableObjectFromScene(this, new Vector2Int((int)transform.position.x - 1, (int)transform.position.z));
                EntryPoint.Instance.LevelsManager.CurrentLevel.AddDestroyableObjectFromScene(this, new Vector2Int((int)transform.position.x, (int)transform.position.z));
                EntryPoint.Instance.LevelsManager.CurrentLevel.AddDestroyableObjectFromScene(this, new Vector2Int((int)transform.position.x + 1, (int)transform.position.z));
                break;
            case TypeDestroyableObject.Mine:
                EntryPoint.Instance.LevelsManager.CurrentLevel.AddDestroyableObjectFromScene(this, new Vector2Int((int)transform.position.x, (int)transform.position.z));
                EntryPoint.Instance.LevelsManager.CurrentLevel.AddDestroyableObjectFromScene(this, new Vector2Int((int)transform.position.x, (int)transform.position.z + 1));
                EntryPoint.Instance.LevelsManager.CurrentLevel.AddDestroyableObjectFromScene(this, new Vector2Int((int)transform.position.x, (int)transform.position.z + 2));
                EntryPoint.Instance.LevelsManager.CurrentLevel.AddDestroyableObjectFromScene(this, new Vector2Int((int)transform.position.x + 1, (int)transform.position.z));
                EntryPoint.Instance.LevelsManager.CurrentLevel.AddDestroyableObjectFromScene(this, new Vector2Int((int)transform.position.x + 1, (int)transform.position.z + 1));
                EntryPoint.Instance.LevelsManager.CurrentLevel.AddDestroyableObjectFromScene(this, new Vector2Int((int)transform.position.x + 1, (int)transform.position.z + 2));
                break;
            default:
                EntryPoint.Instance.LevelsManager.CurrentLevel.AddDestroyableObjectFromScene(this, new Vector2Int((int)transform.position.x, (int)transform.position.z));
                break;
        }
    }

    public void GoblinNotWorkThereMore(Goblin goblin)
    {
        if (_goblins.Contains(goblin))
            _goblins.Remove(goblin);
    }
    public void GoblinStartWorkThere(Goblin goblin)
    {
        _goblins.Add(goblin);
    }
    private void ReleaseAllGoblinsWhereDuretionOver()
    {
        for (int i = _goblins.Count - 1; i >= 0; i--)
        {
            _goblins[i].TryFindNewTarget();
            GoblinNotWorkThereMore(_goblins[i]);
        }
    }

    public Vector2Int GetPosition()
    {
        return _thisPosition;
    }
    public Vector3 GetTargetForGoblins()
    {
        return _targetForGoblins.transform.position;
    }

    public int Level
    {
        set
        {
            _level = value;
            _lvlText.text = _level.ToString();
        }
        get
        {
            return _level;
        }
    }

    private void Update()
    {
        if (_perSecConsumeDurability > 0)
        {
            _perSecConsumeDurability = 0;
        }
        else
        {
            _circleProgressBarText.gameObject.SetActive(false);
        }
    }

    public void ConsumeDurability(double goblinPower)
    {
        //Summ all powers for calculate correct time
        _perSecConsumeDurability += goblinPower;
        if (!_alreadyStarted)
        {
            _circleProgressBar.SetActive(true);
            _lvlCircleBG.SetActive(false);
            _alreadyStarted = true;
        }

        _currentDurability -= goblinPower * Time.deltaTime;
        if (_currentDurability > 0)
        {
            _circleAndText.SetRadial((float)((1 - _currentDurability / _maxDurability)));
            _circleProgressBarText.gameObject.SetActive(true);  
            _circleProgressBarText.text = TimeConv.Format(_currentDurability / _perSecConsumeDurability);
        }
        else
        {
            DurabilityOver();
        }
    }
    private void OnDestroy()
    {
        switch (Type)
        {
            case TypeDestroyableObject.Gate:
                EntryPoint.Instance.LevelsManager.CurrentLevel.RemoveDestroyableObjectFromScene(new Vector2Int((int)transform.position.x - 2, (int)transform.position.z));
                EntryPoint.Instance.LevelsManager.CurrentLevel.RemoveDestroyableObjectFromScene(new Vector2Int((int)transform.position.x + 1, (int)transform.position.z));
                EntryPoint.Instance.LevelsManager.CurrentLevel.RemoveDestroyableObjectFromScene(new Vector2Int((int)transform.position.x, (int)transform.position.z));
                EntryPoint.Instance.LevelsManager.CurrentLevel.RemoveDestroyableObjectFromScene(new Vector2Int((int)transform.position.x + 1, (int)transform.position.z));
                EntryPoint.Instance.LevelsManager.CurrentLevel.RemoveDestroyableObjectFromScene(new Vector2Int((int)transform.position.x + 2, (int)transform.position.z));
                break;
            case TypeDestroyableObject.Exit:
                EntryPoint.Instance.LevelsManager.CurrentLevel.RemoveDestroyableObjectFromScene(new Vector2Int((int)transform.position.x - 1, (int)transform.position.z));
                EntryPoint.Instance.LevelsManager.CurrentLevel.RemoveDestroyableObjectFromScene(new Vector2Int((int)transform.position.x, (int)transform.position.z));
                EntryPoint.Instance.LevelsManager.CurrentLevel.RemoveDestroyableObjectFromScene(new Vector2Int((int)transform.position.x + 1, (int)transform.position.z));
                ActionWhenDurabilityIsOver?.Invoke();
                break;
            case TypeDestroyableObject.Mine:
                EntryPoint.Instance.LevelsManager.CurrentLevel.RemoveDestroyableObjectFromScene(new Vector2Int((int)transform.position.x + 1, (int)transform.position.z));
                EntryPoint.Instance.LevelsManager.CurrentLevel.RemoveDestroyableObjectFromScene(new Vector2Int((int)transform.position.x + 1, (int)transform.position.z + 1));
                EntryPoint.Instance.LevelsManager.CurrentLevel.RemoveDestroyableObjectFromScene(new Vector2Int((int)transform.position.x + 1, (int)transform.position.z + 2));
                EntryPoint.Instance.LevelsManager.CurrentLevel.RemoveDestroyableObjectFromScene(new Vector2Int((int)transform.position.x, (int)transform.position.z));
                EntryPoint.Instance.LevelsManager.CurrentLevel.RemoveDestroyableObjectFromScene(new Vector2Int((int)transform.position.x, (int)transform.position.z + 1));
                EntryPoint.Instance.LevelsManager.CurrentLevel.RemoveDestroyableObjectFromScene(new Vector2Int((int)transform.position.x, (int)transform.position.z + 2));
                break;
            default:
                Vector2Int pos = new Vector2Int((int)transform.position.x, (int)transform.position.z);
                EntryPoint.Instance.LevelsManager.CurrentLevel.RemoveDestroyableObjectFromScene(pos);
                break;
        }

        for (int i = _listGOForDestroy.Count - 1; i >= 0; i--)
        {
            Destroy(_listGOForDestroy[i]);
        }

        ReleaseAllGoblinsWhereDuretionOver();
    }

    private void DurabilityOver()
    {
        ActionWhenDurabilityIsOver?.Invoke();
        
        EffectsAudioManager.PlaySoundBrokenStone();
        Destroy(this);
    }
}
