using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CircleAndText : MonoBehaviour
{
    [SerializeField] private GameObject _circleFill;
    [SerializeField] private GameObject _textGO;
    [SerializeField] private bool _fillRadial = false;

    private CircleProgress _circle;
    private TextMeshProUGUI _text;
    private void Awake()
    {
        _text = _textGO.GetComponent<TextMeshProUGUI>();
        _circle = new CircleProgress(_circleFill.GetComponent<Image>(), _fillRadial);
    }

    public void SetRadial(float amount)
    {
        _circle.SetRadial(amount);
    }

    public void SetText(string text)
    {
        _textGO.SetActive(true);
        _text.text = text;
    }

    public void SetTime(double timer, bool large = false)
    {
        if (_text == null) return;

        _textGO.SetActive(true);
        if (large)
        {
            _text.text = TimeConv.FormatForCannon(timer);
        }
        else
        {
            _text.text = TimeConv.Format(timer);
        }
    }

}
