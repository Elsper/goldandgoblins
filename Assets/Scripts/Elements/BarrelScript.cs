using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelScript : MonoBehaviour
{

    public ParticleSystem boom;
    public ParticleSystem puff;
    public GameObject goblin;
    public GameObject gold;
    Vector2Int pos;
    private int _goblinLevel;

    public int GoblinLevel { get => _goblinLevel; set => _goblinLevel = value; }

    public void InitBarrel(int goblinLevel)
    {
        EffectsAudioManager.PlaySoundLandingBarrel();
        _goblinLevel = goblinLevel;
        pos = new Vector2Int((int)transform.position.x, (int)transform.position.z);
        EntryPoint.Instance.LevelsManager.CurrentLevel.AddBarrelObjectFromScene(this, pos);
        EntryPoint.Instance.GoblinsManager.AddedGoblinOrBarrel();
        GetComponent<ObstacleDrag>().ResetAllowablePositions();
    }

    public void OnMouseDown()
    {
        EffectsAudioManager.PlaySoundDestructionBarrel();
        GoblinAudioManager.PlayVoiceSoudSpawn();

        Instantiate(boom, transform.position, Quaternion.identity);
        Instantiate(puff, transform.position, Quaternion.identity);
        Destroy(gameObject);

        GameObject GOGoblin = Instantiate(goblin, transform.position, Quaternion.identity, EntryPoint.Instance.LevelsManager.CurrentLevelGO.gameObject.transform);
        Goblin NewGoblin = GOGoblin.GetComponent<Goblin>();

        NewGoblin.SetLevel(_goblinLevel);
    }

    private void OnDestroy()
    {
        EntryPoint.Instance.LevelsManager.CurrentLevel.RemoveBarrelObjectFromScene(pos);
    }
}

