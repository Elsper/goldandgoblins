﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public static class Balance 
{
    static Balance()
    {
        InitBalance();
    }
    private static void InitBalance()
    {
        SideMineInitBalance();
        ForgeInitBalance();
    }

    //SideMines
    public class UpgradeBorderValue
    {
        public int Myltiply = 2;
        public int Level = 10;

        public UpgradeBorderValue(int mylty, int lvl)
        {
            Myltiply = mylty;
            Level = lvl;
        }
    }
    public static Dictionary<Gems, List<UpgradeBorderValue>> MineSideBorderValues;
    public static Dictionary<Gems, double> SideMineStartIncome;
    public static void SideMineInitBalance()
    {
        SideMineStartIncome = new Dictionary<Gems, double>();

        SideMineStartIncome[Gems.amethist] = 400;
        SideMineStartIncome[Gems.citraine] = 6750;
        SideMineStartIncome[Gems.agate] = 140625;
        SideMineStartIncome[Gems.topaz] = 3400000;
        SideMineStartIncome[Gems.opal] = 95200000;

        //other balance
        //for (int i = 5; i < 15; i++)
        //    SideMineStartIncome[i] = SideMineStartIncome[i - 1] * (i * 4 + 8.5);

        MineSideBorderValues = new Dictionary<Gems, List<UpgradeBorderValue>>();
        foreach (Gems item in Enum.GetValues(typeof(Gems)))
        {
            MineSideBorderValues[item] = new List<UpgradeBorderValue>();
        }

        Gems _currentIndex = Gems.amethist;
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(2, 10)); //+
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(2, 20)); //+
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(2, 40)); //+
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(200, 60));//+
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(30, 80));//
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(50, 100));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(10, 120));

        _currentIndex = Gems.citraine;
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(3, 10)); //+
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(3, 20)); //+
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(3, 30));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(3, 40));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(10, 60));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(3, 80));//+
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(50, 100));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(10, 120));

        _currentIndex = Gems.agate;
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(2, 10)); //+
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(3, 20)); //+
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(3, 30));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(3, 40));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(10, 60));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(2, 80)); //+
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(50, 100));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(10, 120));

        _currentIndex = Gems.topaz;
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(2, 10));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(2, 20));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(3, 30));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(3, 40));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(10, 60));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(10, 80));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(50, 100));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(10, 120));

        _currentIndex = Gems.opal;
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(2, 10));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(2, 20));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(3, 30));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(3, 40));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(10, 60));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(10, 80));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(50, 100));
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(10, 120));

        /*
        //Шестая шахта (по цвету камня в оригинале)
        _currentIndex = 5;
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(2, 10)); //9
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(10, 20)); //454
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(2, 40)); // 466
        MineSideBorderValues[_currentIndex].Add(new UpgradeBorderValue(100, 60)); // 479*/
    }

    public static float SideMineMultyPricePerLVL(Gems index)
    {
        return 1.43f + (int)index * 0.03f;
    }
    public static double SideMineStartPrice(Gems index)
    {
        return 64 * Math.Pow(8, (int)index);
    }
    public static float SideMineSpeed(Gems index)
    {
        return 7.5f * Mathf.Pow(2,(int)index);
    }
    public static double SideMineIncomePerTime(Gems mineType)
    {
        var level = EntryPoint.Instance.MineSidesManager.GetLevel(mineType);
        int multiply = 1;
        
        foreach (var item in MineSideBorderValues[mineType])
        {
            if (item.Level <= level) multiply *= item.Myltiply;
        }

        //Заглушка для больших уровней
        int deltaLVL = level - MineSideBorderValues[mineType][MineSideBorderValues[mineType].Count - 1].Level;
        if (deltaLVL > 10)
            multiply = multiply * (int)(Math.Pow(2, deltaLVL / 10));

        multiply *= EntryPoint.Instance.MineSidesManager.GetMyltyplyIncome(mineType);

        return SideMineStartIncome[mineType] * level * multiply;
    }

    //Durability
    public static double DurabilityForStone(int lvl, TypeDestroyableObject type)
    {
        switch (type)
        {
            case TypeDestroyableObject.Gate:
                return 50 * Math.Pow(3, lvl - 1);
            case TypeDestroyableObject.Exit:
                return 50 * Math.Pow(3, lvl - 1);
            case TypeDestroyableObject.Mine:
                return 50 * Math.Pow(3, lvl - 1);
            default:
                return 10 * Math.Pow(3, lvl - 1);
        }
    }

    //Forge
    public static List<UpgradeBorderValue> ForgeBorderValues;
    public static void ForgeInitBalance()
    {
        ForgeBorderValues = new List<UpgradeBorderValue>();
        ForgeBorderValues.Add(new UpgradeBorderValue(2, 10)); //+
        ForgeBorderValues.Add(new UpgradeBorderValue(2, 20)); //+
        ForgeBorderValues.Add(new UpgradeBorderValue(2, 30)); //+
        ForgeBorderValues.Add(new UpgradeBorderValue(10, 40)); //+
        ForgeBorderValues.Add(new UpgradeBorderValue(2, 50)); //+
        ForgeBorderValues.Add(new UpgradeBorderValue(2, 60)); //+
        ForgeBorderValues.Add(new UpgradeBorderValue(2, 70)); //+
        ForgeBorderValues.Add(new UpgradeBorderValue(100, 80)); //+
        ForgeBorderValues.Add(new UpgradeBorderValue(2, 90)); //+
        ForgeBorderValues.Add(new UpgradeBorderValue(2, 100));//+
        ForgeBorderValues.Add(new UpgradeBorderValue(10, 110));
        ForgeBorderValues.Add(new UpgradeBorderValue(1000, 120)); //+
        ForgeBorderValues.Add(new UpgradeBorderValue(10, 140));
        ForgeBorderValues.Add(new UpgradeBorderValue(10, 160));
        ForgeBorderValues.Add(new UpgradeBorderValue(10, 180));
        ForgeBorderValues.Add(new UpgradeBorderValue(10, 200));
    }
    public static float ForgeSpeed = 2f;
    public static float ForgeBaseIncome = 3f;
    public static float ForgeBasePrice = 72f;
    public static float ForgeMyltiplyPricePerLevel = 1.43f;

    public static double ForgeMoneyIncomePerTime = 3;
    public static double ForgeGetIncome(int lvl)
    {
        int multiply = 1;
        foreach (var item in ForgeBorderValues)
        {
            if (item.Level <= lvl) multiply *= item.Myltiply;
        }

        //Заглушка для больших уровней
        int deltaLVL = lvl - ForgeBorderValues[ForgeBorderValues.Count - 1].Level;
        if (deltaLVL > 10)
            multiply = multiply * (int)(Math.Pow(2, deltaLVL / 10));

        ForgeMoneyIncomePerTime = lvl * ForgeBaseIncome* multiply;

        ForgeMoneyIncomePerTime *= 1 + EntryPoint.Instance.CardsManager.GetBonusForgeFromCard();

        return ForgeMoneyIncomePerTime;
        //TODO добавить прокачку от карточек
    }

    //Cannon
    public static float CannonSpeed = 1800f;
    public static float CannonSpeedFastShot = 30f;

    //Goblin
    public static double GoblinGetPowerByLevel(int lvl)
    {
        return Math.Pow(2.1d, lvl - 1);
    }
    
    
}
