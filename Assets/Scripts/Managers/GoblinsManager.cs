using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveLoadGoblinsState
{
    public class SLGoblin
    {
        public int Level;
        public Vector2Int Pos;
        public Vector2Int TargetPos;

        [JsonConstructor]
        public SLGoblin() { }
        public SLGoblin(Goblin goblin, Vector2Int pos)
        {
            Level = goblin.GetLevel();
            Pos = pos;
            TargetPos = goblin.GetTargetPos();
        }
    }

    public List<SLGoblin> SLGoblins = new List<SLGoblin>();
    public int CountBuyGoblins;
    public int CountGoblinsAndBarrels;

    public SaveLoadGoblinsState()
    {
        SLGoblins = new List<SLGoblin>();
    }
}

public class GoblinsManager : MonoBehaviour
{
    [SerializeField] Goblin _goblinPrefab;

    private Dictionary<Vector2Int, Goblin> _allGoblins = new Dictionary<Vector2Int, Goblin>();

    private SaveLoadGoblinsState _saveLoadGoblinsState;

    private const int _baseLimit = 10;
    private int _countGoblinsAndBarrels;
    private int _countBuyGoblins;

    public event Action<int, int> OnChangeCountGoblins;

    public int Limit { get { return _baseLimit + EntryPoint.Instance.CardsManager.GetBonusLimitFromCards(); } }
    public int CountGoblinsAndBarrels
    {
        get => _countGoblinsAndBarrels;
        set
        {
            _countGoblinsAndBarrels = value;
            OnChangeCountGoblins?.Invoke(CountGoblinsAndBarrels, Limit);
        }
    }
    public void Init()
    {
        _countGoblinsAndBarrels = 0;
        _countBuyGoblins = 0;
    }

    public SaveLoadGoblinsState SaveLoadGoblinsState
    {
        get
        {
            _saveLoadGoblinsState = new SaveLoadGoblinsState(); 
            return GetSaveData();
        }
        set
        {
            _saveLoadGoblinsState = value; 
            LoadFromSaveData();
        }
    }
    private SaveLoadGoblinsState GetSaveData()
    {
        foreach (var item in _allGoblins)
        {
            _saveLoadGoblinsState.SLGoblins.Add(new SaveLoadGoblinsState.SLGoblin(item.Value, item.Key));
        }
        _saveLoadGoblinsState.CountBuyGoblins = _countBuyGoblins;
        _saveLoadGoblinsState.CountGoblinsAndBarrels = CountGoblinsAndBarrels;

        return _saveLoadGoblinsState;
    }
    private void LoadFromSaveData()
    {
        if (_saveLoadGoblinsState == null)
            return;

        List<Vector2Int> goblinsKeys = new List<Vector2Int>(_allGoblins.Keys);
        foreach (var item in goblinsKeys)
        {
            if (_allGoblins[item]==null)
            {
                Debug.Log("error with goblins");
                _allGoblins.Remove(item);
            }
            else
            GameObject.DestroyImmediate(_allGoblins[item].gameObject);
        }
        foreach (var item in _saveLoadGoblinsState.SLGoblins)
        {
            Instantiate(_goblinPrefab, new Vector3(item.Pos.x, 0, item.Pos.y), Quaternion.identity, EntryPoint.Instance.LevelsManager.CurrentLevelGO.gameObject.transform)
                .SetLevel(item.Level);
            _allGoblins[item.Pos]?.TryFindNewTarget(true, item.TargetPos);
        }
        CountBuyGoblins = _saveLoadGoblinsState.CountBuyGoblins;
        CountGoblinsAndBarrels = _saveLoadGoblinsState.CountGoblinsAndBarrels;
    }

    public void ResetWhenStartNewLevel()
    {
        CountBuyGoblins = 0;
    }
    public bool IsHaveLimitForNewBarrel()
    {
        return CountGoblinsAndBarrels < Limit ? true : false;
    }

    public int CountBuyGoblins
    {
        get { return _countBuyGoblins; }
        set
        {
            _countBuyGoblins = value;
            EntryPoint.Instance.UIManager.UIBottomPanel.UpdateCenterBtn();
        }
    }
    public void AddedGoblinOrBarrel() => CountGoblinsAndBarrels++;
    public void GoblinsWasRemoved() => CountGoblinsAndBarrels--;
    public bool IsGoblinExistOnScene(Vector2Int pos)
    {
        if (_allGoblins.ContainsKey(pos))
            return true;
        else
            return false;
    }
    public void AddGoblinFromScene(Goblin goblin, Vector2Int pos)
    {
        if (!_allGoblins.ContainsKey(pos))
        {
            _allGoblins[pos] = goblin;
        }
    }
    public void RemoveGoblinFromScene(Vector2Int pos)
    {
        _allGoblins.Remove(pos);
        //Debug.Log(_allGoblins.Count);
    }

    public double PriceForBuyGoblin
    {
        get { return 2 * Math.Pow(4, _countBuyGoblins + 1); }
    }


    public void MakeWorks()
    {
        foreach (Goblin item in _allGoblins.Values)
        {
            item?.MakeWork();
        }
    }

}
