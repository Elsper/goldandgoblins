﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class LevelsManager : MonoBehaviour
{
    [SerializeField] private Level[] _levels;

    [SerializeField] private Level _eventLevelPrefab;

    [Header("BaseLevels")]
    private Cannon _cannon;

    private bool _isEventLevel = false;

    private int _nomerLevel = 0;
    private Level _normalLevel;
    private Level _eventLevel;
    private GameObject _currentLevelGO;

    public event Action<int> OnChangeLevel;


    private SaveLoadLevelState _slLevelStateBase;
    private SaveLoadLevelState _slLevelStateEvent;

    public void Init()
    {
        NomerLevel = 0;
        TryMakeNextLevel();
        EventLevel = _eventLevelPrefab;
        EntryPoint.Instance.CurrencyManager.Coins.OnChangeCount += OnChangeCountCoins;

    }

    public void OnChangeCountCoins(double count) => CurrentLevel.CountCoinsThisLevel = count;
    public Cannon GetCannon()
    {
        if (_cannon == null)
            _cannon = FindObjectOfType<Cannon>();

        return _cannon;
    }
    public int NomerLevel { get => _nomerLevel; set => _nomerLevel = value; }
    public GameObject CurrentLevelGO { get => _currentLevelGO; set => _currentLevelGO = value; }
    public Level CurrentLevel
    {
        get
        {
            if (IsEventLevel)
                return EventLevel;
            else
                return NormalLevel;
        }
        set
        {
            if (IsEventLevel)
                EventLevel = value;
            else
                NormalLevel = value;
        }
    }

    public Level NormalLevel { get => _normalLevel; set => _normalLevel = value; }
    public Level EventLevel { get => _eventLevel; set => _eventLevel = value; }
    public bool IsEventLevel { get => _isEventLevel; set => _isEventLevel = value; }
    public SaveLoadLevelState SLLevelStateEvent { get => _slLevelStateEvent; set => _slLevelStateEvent = value; }
    public SaveLoadLevelState SLLevelStateBase { 
        get => _slLevelStateBase; 
        set => _slLevelStateBase = value; 
    }

    public void TryMakeNextLevel()
    {
        _nomerLevel++;
        TryLoadBaseLevel(_nomerLevel,true);
    }

    public void TryLoadBaseLevel(int nomerLevel, bool makeWithoutLoad = false)
    {
        _nomerLevel = nomerLevel;
        if ((_nomerLevel < 1) || (_nomerLevel > _levels.Length)) _nomerLevel = 1;
        if (_levels.Length <= _nomerLevel - 1)
        {
            Debug.Log("No finded levels for loading");
            return;
        }

        MakeLevel(false, makeWithoutLoad);
    }
    public void MakeLevel(bool eventLevel, bool makeWithoutLoad = false)
    {
        //Запоминаем старые данные.
        PrepareSaveData();
        Level prefabLevel;
        if (eventLevel)
        {
            prefabLevel = _eventLevelPrefab;
        }
        else
        {
            prefabLevel = _levels[_nomerLevel - 1];
        }
        //Удаляем
        if (CurrentLevelGO != null)
            DestroyExistLevel();

        //Начинам новую генерацию
        IsEventLevel = eventLevel;

        EntryPoint.Instance.CurrencyManager.Coins.SetCount(0);
        EntryPoint.Instance.GoblinsManager.ResetWhenStartNewLevel();

        CurrentLevel = prefabLevel;

        CurrentLevelGO = Instantiate(prefabLevel.gameObject);

        CurrentLevel.LevelForMines = CurrentLevelGO.GetComponent<Level>().LevelForMines;

        if (!makeWithoutLoad)
            LoadBySaveData();
    }
    public void PrepareSaveData()
    {
        SaveLoadLevelState normalLevel = null;
        SaveLoadLevelState eventLevel = null;
        if (NormalLevel != null)
            normalLevel = NormalLevel.GetSaveData();
        if (EventLevel != null)
            eventLevel = EventLevel.GetSaveData();

        if (normalLevel != null) SLLevelStateBase = normalLevel;
        if (eventLevel != null) SLLevelStateEvent = eventLevel;
    }
    public void LoadBySaveData()
    {
        if (IsEventLevel)
            CurrentLevel.SLLevelState = SLLevelStateEvent;
        else
            CurrentLevel.SLLevelState = SLLevelStateBase;

        CurrentLevel.LoadLevelFromSaveData();

        OnChangeLevel?.Invoke(_nomerLevel);
    }
    private void DestroyExistLevel() => DestroyImmediate(CurrentLevelGO);
}
