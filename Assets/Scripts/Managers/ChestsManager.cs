using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ChestType
{
    Wood,
    Iron,
    Gold
}
public enum ChestTextures
{
    ChestWoodClosed,
    ChestIronClosed,
    ChestGoldClosed,
    ChestWoodOpen,
    ChestIronOpen,
    ChestGoldOpen
}

public class OneChest
{
    private Texture _textureClosed;
    private Texture _textureOpen;

    private ChestControler _chestController;
    private string _caption;
    private Dictionary<RareCard, int> _countCards;

    public OneChest(Texture textureClosed, Texture textureOpen, ChestControler chestController, string caption, Dictionary<RareCard, int> countCards)
    {
        _textureClosed = textureClosed;
        _textureOpen = textureOpen;
        _chestController = chestController;
        _caption = caption;
        _countCards = countCards;
    }

    public Texture ClosedTexture { get { return _textureClosed; } }
    public Texture OpenTexture { get { return _textureOpen; } }
    public ChestControler ChestControler { get { return _chestController; } }
    public string Caption { get { return _caption; } }
    public Dictionary<RareCard, int> CountCards { get { return _countCards; } }

    public int TotalCountCards
    {
        get
        {
            int count = 0;
            foreach (var item in _countCards.Values)
            {
                count += item;
            }
            return count;
        }
    }
}
public class OneCardChestReward
{
    public OneCard OneCard;
    public int Count;

    public OneCardChestReward(OneCard oneCard, int count)
    {
        OneCard = oneCard;
        Count = count;
    }
}

public class ChestsManager : MonoBehaviour
{
    [Header("Textures")]
    [SerializeField] private Texture _chestWoodClosed;
    [SerializeField] private Texture _chestIronClosed;
    [SerializeField] private Texture _chestGoldClosed;
    [SerializeField] private Texture _chestWoodOpen;
    [SerializeField] private Texture _chestIronOpen;
    [SerializeField] private Texture _chestGoldOpen;

    [Header("Chests")]
    [SerializeField] private ChestControler _chestWood;
    [SerializeField] private ChestControler _chestIron;
    [SerializeField] private ChestControler _chestGold;

    public Dictionary<ChestType, OneChest> AllChests;
    public List<OneCardChestReward> ListCardsRewards;

    public event Action<int> OnChangeCountFreeChests;

    public readonly int NeedSecondForNextChest = 240 * 60; //4h

    private int _timeForFullFreeChests;

    private int _countFreeChests = -1;
    private bool _coroutineStarted = false;
    public void Init()
    {
        AllChests = new Dictionary<ChestType, OneChest>();

        AllChests[ChestType.Wood] = new OneChest(_chestWoodClosed, _chestWoodOpen, _chestWood, "���������� ������", DictCountByRare(ChestType.Wood));
        AllChests[ChestType.Iron] = new OneChest(_chestIronClosed, _chestIronOpen, _chestIron, "�������� ������", DictCountByRare(ChestType.Iron));
        AllChests[ChestType.Gold] = new OneChest(_chestGoldClosed, _chestGoldOpen, _chestGold, "������� ������", DictCountByRare(ChestType.Gold));
    }

    public void OnLoadData()
    {
        if (_timeForFullFreeChests < SpecialFunctions.GetNowTotalSeconds())
            _timeForFullFreeChests = SpecialFunctions.GetNowTotalSeconds();

        if (!_coroutineStarted)
        {
            StartCoroutine(onCheckFreeChestCount());
            _coroutineStarted = true;
        }
    }

    public int CountFreeChests
    {
        get { return _countFreeChests; }
        set
        {
            if (_countFreeChests != value)
            {
                _countFreeChests = value;
                OnChangeCountFreeChests?.Invoke(value);
            }
            else
                _countFreeChests = value;
        }
    }

    public int TimeForFullFreeChests { get => _timeForFullFreeChests; set => _timeForFullFreeChests = value; }

    IEnumerator onCheckFreeChestCount()
    {
        while (true)
        {
            var deltaTimeForFullFreeChest = TimeForFullFreeChests - SpecialFunctions.GetNowTotalSeconds();
            if (deltaTimeForFullFreeChest <= 0)
                CountFreeChests = 2;
            else
                CountFreeChests = 1 - (int)(deltaTimeForFullFreeChest / NeedSecondForNextChest);
            yield return new WaitForSeconds(1f);
        }
    }

    public string TimeForNextFreeChest() =>
        TimeConv.Format(((TimeForFullFreeChests - SpecialFunctions.GetNowTotalSeconds()) % NeedSecondForNextChest));


    public void AddFreeTimeForFullFreeChests()
    {
        if (_timeForFullFreeChests < SpecialFunctions.GetNowTotalSeconds())
            _timeForFullFreeChests = SpecialFunctions.GetNowTotalSeconds();

        if (CountFreeChests>0)
            TimeForFullFreeChests += NeedSecondForNextChest;
    }

    public void AddCurentRewardsToCards()
    {
        foreach (var item in ListCardsRewards)
        {
            EntryPoint.Instance.CardsManager.Cards.AllCardsData[item.OneCard.Index].CountHaveCards += item.Count;
        }
    }
    public void GenerateRewards(ChestType chest)
    {
        ListCardsRewards = new List<OneCardChestReward>();

        ListCardsRewards.Add(new OneCardChestReward(EntryPoint.Instance.CardsManager.GetCardForChest(RareCard.Common), (int)(AllChests[chest].CountCards[RareCard.Common] * 0.6f)));
        ListCardsRewards.Add(new OneCardChestReward(EntryPoint.Instance.CardsManager.GetCardForChest(RareCard.Common), (int)(AllChests[chest].CountCards[RareCard.Common] * 0.4f)));
        if (ListCardsRewards[1].OneCard == ListCardsRewards[0].OneCard)
        {
            ListCardsRewards[0].Count += ListCardsRewards[1].Count;
            ListCardsRewards.RemoveAt(1);
        }

        if (AllChests[chest].CountCards.ContainsKey(RareCard.Uncommon))
        {
            ListCardsRewards.Add(new OneCardChestReward(EntryPoint.Instance.CardsManager.GetCardForChest(RareCard.Uncommon), AllChests[chest].CountCards[RareCard.Uncommon]));
        }
        if (AllChests[chest].CountCards.ContainsKey(RareCard.Rare))
        {
            ListCardsRewards.Add(new OneCardChestReward(EntryPoint.Instance.CardsManager.GetCardForChest(RareCard.Rare), AllChests[chest].CountCards[RareCard.Rare]));
        }
        if (AllChests[chest].CountCards.ContainsKey(RareCard.Unique))
        {
            ListCardsRewards.Add(new OneCardChestReward(EntryPoint.Instance.CardsManager.GetCardForChest(RareCard.Unique), AllChests[chest].CountCards[RareCard.Unique]));
        }

    }

    private Dictionary<RareCard, int> DictCountByRare(ChestType chest)
    {
        Dictionary<RareCard, int> DictResult = new Dictionary<RareCard, int>();
        switch (chest)
        {
            case ChestType.Wood:
                DictResult[RareCard.Common] = 9;
                DictResult[RareCard.Uncommon] = 1;
                break;
            case ChestType.Iron:
                DictResult[RareCard.Common] = 18;
                DictResult[RareCard.Uncommon] = 2;
                break;
            case ChestType.Gold: //� ��������� ��� ��������� ������������ �������
                DictResult[RareCard.Common] = 112;
                DictResult[RareCard.Uncommon] = 28;
                DictResult[RareCard.Rare] = 11;
                break;
            default:
                Debug.Log("Unexpected chest");
                break;
        }

        return DictResult;
    }

}