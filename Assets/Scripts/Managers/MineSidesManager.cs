using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Gems
{
    amethist,
    citraine,
    agate,
    topaz,
    opal
}

public struct DemandsCardForAuto
{
    public OneCard Card;
    public int Level;
}

public class MineSideData
{
    public int Level { get; set; }
    public double CurrentMiningTime { get; set; }
    public bool IsAuto { get; set; }
    public bool IsActivated { get; set; }

    [JsonConstructor]
    public MineSideData() { }
    public MineSideData(int level, double currentMiningTime, bool isAuto, bool isActivated)
    {
        Level = level;
        CurrentMiningTime = currentMiningTime;
        IsAuto = isAuto;
        IsActivated = isActivated;
    }
}


public class SaveLoadMineState
{
    public Dictionary<Gems, MineSideData> AllMineSideData;

    public SaveLoadMineState()
    {
        AllMineSideData = new Dictionary<Gems, MineSideData>();
    }
}


public class MineSidesManager
{
    private Dictionary<Gems, MineSideData> _allMineSideData;
    private Dictionary<Gems, MineSide> _allMineSideOnLevel;

    private SaveLoadMineState _saveLoadMineState; 

    public MineSidesManager()
    {
        ResetData();
    }


    public SaveLoadMineState SaveLoadMineState
    {
        get
        {
            _saveLoadMineState = new SaveLoadMineState();
            return GetSaveData();
        }
        set
        {
            _saveLoadMineState = value;
            LoadFromSaveData();
        }
    }
    private SaveLoadMineState GetSaveData()
    {
        foreach (Gems item in Enum.GetValues(typeof(Gems))) 
        {
            if (!_allMineSideOnLevel.ContainsKey(item))
            {
                _saveLoadMineState.AllMineSideData.Remove(item);
                continue;
            }

            _allMineSideData[item].CurrentMiningTime = _allMineSideOnLevel[item].GetTimeForTimerMining();
            _saveLoadMineState.AllMineSideData[item] = _allMineSideData[item];
        }
        return _saveLoadMineState;
    }
    private void LoadFromSaveData()
    {
        if (_saveLoadMineState == null)
            return;

        _allMineSideData = _saveLoadMineState.AllMineSideData;

        foreach (Gems item in Enum.GetValues(typeof(Gems)))
        {
            if (!_allMineSideData.ContainsKey(item))
            {
                _allMineSideOnLevel.Remove(item);
                continue;
            }
            if (!_allMineSideOnLevel.ContainsKey(item))
                continue;
            if (_allMineSideOnLevel[item]==null)
                continue;

            if (_allMineSideData[item].IsActivated)
            _allMineSideOnLevel[item].OnLoadActivationWithLevel(_allMineSideData[item].Level);
            if (_allMineSideData[item].IsAuto)
                _allMineSideOnLevel[item].SetAutomatization();
            _allMineSideOnLevel[item].SetTimeForTimerMining(_allMineSideData[item].CurrentMiningTime);


        }
    }

    public void AddMineFromScene(Gems type, MineSide mineSide)
    {
        if (_allMineSideData == null) 
            _allMineSideData = new Dictionary<Gems, MineSideData>();

            _allMineSideOnLevel[type] = mineSide;
            _allMineSideData[type] = new MineSideData(1,0,false, false);
        
    }



    public void ResetWhenStartNewLevel() => ResetData();
    public void ResetData()
    {
        _allMineSideOnLevel = new Dictionary<Gems, MineSide>();
        _allMineSideData = new Dictionary<Gems, MineSideData>();

        foreach (Gems item in Enum.GetValues(typeof(Gems)))
        {
            MineSideData _data = new()
            {
                CurrentMiningTime = 0,
                Level = 1
            };

            _allMineSideData[item] = _data;
        }
    }

    public Dictionary<Gems, MineSideData> AllMineSideData
    {
        get { 
            return _allMineSideData; 
        }
        set { 
            _allMineSideData = value; 
        }
    }

    public string GetCaptionBeGem(Gems gem)
    {
        return gem switch
        {
            Gems.amethist => "�������",
            Gems.citraine => "������",
            Gems.agate => "����",
            Gems.topaz => "�����",
            Gems.opal => "����",
            _ => throw new System.NotImplementedException(),
        };
    }

    public int GetLevel(Gems type) => _allMineSideData[type].Level;
    public void SetLevel(Gems type, int value) => _allMineSideData[type].Level = value;

    public DemandsCardForAuto GetDemandsCardForAuto(Gems type)
    {
        int level = 1+EntryPoint.Instance.LevelsManager.CurrentLevel.LevelForMines / 2;
        if (level < 1) level = 1;

        return
            new DemandsCardForAuto()
            {
                Level = level,
                Card = EntryPoint.Instance.CardsManager.Cards.AllCardsData[EntryPoint.Instance.CardsManager.GetIndex(type, (EntryPoint.Instance.LevelsManager.CurrentLevel.LevelForMines+1) % 2)]
            };
    }

    public int GetMyltyplyIncome(Gems TypeMine)
    {
        int value = 1;
        TypeCard typeCard = EntryPoint.Instance.CardsManager.GetTypeCardByTypeMine(TypeMine);        

        foreach (var item in EntryPoint.Instance.CardsManager.Cards.AllCardsData.Values)
        {
            if (item.Type == typeCard)
                value *= item.GetValueEffectWithCurrentLevel();
        }

        return value;
    }
    public void SetAutomatization(Gems TypeMine) => _allMineSideOnLevel[TypeMine].SetAutomatization();

}
