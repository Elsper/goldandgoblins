﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using UnityEngine;

public class TimerForCircle: MonoBehaviour
{
    private CircleAndText _circleAndText;
    private bool needSetCircle = false;
    private bool _pause = false;
    
    private Dictionary<double, Action> _actionList;
    private Action _actionZeroTime;
    private Action<double> _actionChangeTextByTime;

    private double _timeStart;
    private double _timeStay;

    public double TimeStay { get => _timeStay; set => _timeStay = value; }

    public void InitTimerForCircle(double timeStart, Action action)
    {
        MakeTimer(null, new Dictionary<double, Action> {{0, action }}, timeStart, timeStart);
    }
    public void InitTimerForCircle(double timeStart, Dictionary<double, Action> actionList, CircleAndText circleAndText, Action<double> CircleTextChangeByTime = null)
    {
        MakeTimer(circleAndText, actionList, timeStart, timeStart, CircleTextChangeByTime);
    }

    private void MakeTimer(CircleAndText circleAndText, Dictionary<double, Action> actionList, double timeStart, double timeStay, Action<double> CircleTextChangeByTime = null)
    {
        if (actionList.ContainsKey(0))
        {
            _actionZeroTime = actionList[0];
            actionList.Remove(0);
        }
        if (circleAndText != null)
        {
            _circleAndText = circleAndText;
            needSetCircle = true;
        }
            _actionList = actionList;
        _timeStart = timeStart;
        TimeStay = timeStay;
        _actionChangeTextByTime = CircleTextChangeByTime;
    }

    private void Update()
    {
        if (_pause) return;

        TimeStay -= Time.deltaTime;

        if (needSetCircle)
            _circleAndText.SetRadial((float)((1 - TimeStay / _timeStart)));

        _actionChangeTextByTime?.Invoke(TimeStay);

        foreach (var item in _actionList)
        {
            if (TimeStay <= item.Key) 
                item.Value();
        }

        if (TimeStay <= 0)
        {
            TimeStay = _timeStart;
            _actionZeroTime?.Invoke();
        }
    }

    public void Pause()
    {
        _pause = true;
    }
    public void Unpause()
    {
        _pause = false;
    }
    
}
