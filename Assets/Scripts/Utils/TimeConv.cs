using System;
using UnityEngine;

public static class TimeConv
{
    public static string Format(double seconds) //��� ������
    {

        if (seconds < 10) return 5 + "c.";
        else if (seconds > 50 && seconds < 60) return 1 + "m.";
        else if (seconds <= 50) return Math.Round(seconds / 10) * 10 + "c.";

        else
        {
            TimeSpan ts = TimeSpan.FromSeconds(seconds);
            string[] timeParts = new string[2];
            int countParts=0;

            if ((countParts < 2) && (ts.Days != 0)) { timeParts[countParts] = ts.Days + "� "; countParts++;  }
            if ((countParts < 2) && (ts.Hours != 0)) { timeParts[countParts] = ts.Hours + "� "; countParts++;  }
            if ((countParts < 2) && (ts.Minutes != 0)) { timeParts[countParts] = ts.Minutes + "m "; countParts++;  }
            if ((countParts < 2) && (ts.Seconds != 0) && (Math.Round(ts.Seconds / 10d) * 10!=0)) { timeParts[countParts] = Math.Round(ts.Seconds / 10d) * 10 + "c."; countParts++;  }

            return timeParts[0] + timeParts[1];
            /*
            if (ts.Days != 0) return ts.Days + "�." + ts.Hours + "�.";
            else if (ts.Hours != 0) return ts.Hours + "�." + ts.Minutes + "m.";
            else if (ts.Hours == 0) return ts.Minutes + "m.";
            else if (ts.Minutes != 0) return ts.Minutes + "m.";
            else
            {
                Debug.Log(message: "Invalid Parametr Value");
                return "";
            }*/
        }

    }
    public static string FormatForWheel(double seconds)
    {

        if (seconds < 10) return 5 + "c";
        else if (seconds > 50 && seconds < 60) return 1 + "m";
        else if (seconds <= 50) return Math.Round(seconds / 10) * 10 + "c";

        else
        {
            TimeSpan ts = TimeSpan.FromSeconds(seconds);
            string[] timeParts = new string[2];
            int countParts = 0;

            if ((countParts < 2) && (ts.Hours != 0))
            {
                timeParts[countParts] = ts.Hours + "� "; countParts++;
            }
            if ((countParts < 2) && (ts.Minutes != 0))
            {
                timeParts[countParts] = ts.Minutes + "� "; countParts++;
            }
            if ((countParts < 2) && (ts.Seconds != 0) && (Math.Round(ts.Seconds / 10d) * 10 != 0))
            {
                timeParts[countParts] = Math.Round(ts.Seconds / 10d) * 10 + "�"; countParts++;
            }

            return timeParts[0] + timeParts[1];
        }

    }
    /// <summary>
    /// Format for canon : "Hours+min" or "minuts + sec"
    /// </summary>
    /// <param name="seconds"></param>
    /// <returns></returns>
    public static string FormatForCannon(double seconds) //��� �����
    {
        TimeSpan ts = TimeSpan.FromSeconds(seconds);
        if (ts.Days != 0) return ts.Days + "�" + ts.Hours + "�";
        else if (ts.Hours != 0) return ts.Hours + "� " + ts.Minutes + "m";
        else if (ts.Hours == 0) return ts.Minutes + "m " + ts.Seconds + "c";
        else
        {
            Debug.Log(message: "Invalid Parametr Value");
            return "";
        }

    }
}
