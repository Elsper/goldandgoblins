using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerFlightGold : MonoBehaviour
{
    [SerializeField] private GameObject _objectShutdown;
    [SerializeField] private FlightResource[] _flightGold;

    private float _delayTime=0;
    private void Start()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();

        for (int i = 0; i < _flightGold.Length; i++)
        {
            float _randomNumberX = Random.Range(-8f, 8f);
            float _randomNumberY = Random.Range(-8f, 8f);

            _flightGold[i].transform.position = new Vector2
                (
                _flightGold[i].transform.position.x + _randomNumberX, 
                _flightGold[i].transform.position.y + _randomNumberY
                );

            _flightGold[i].InitAndSetDelayTime(_delayTime, rectTransform.anchoredPosition);
            _delayTime += 0.02f;
        }
        _objectShutdown.SetActive(true);
    }
}
