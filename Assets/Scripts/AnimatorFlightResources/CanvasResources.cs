using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasResources : MonoBehaviour
{
    [SerializeField] private GameObject _canvasPrefab;
    private GameObject _canvasResources;

    public void SpawnCanvas()
    {
        _canvasResources = Instantiate(_canvasPrefab);
    }

    public bool GetIsCanvasAdded()
    {
        return (_canvasResources == null);
    }

    public Transform GetCanvasResources()
    {
        return _canvasResources.transform;
    }

    public Transform GetTargetHeight()
    {
        return _canvasResources.GetComponentsInChildren<Transform>()[1];
    }

}