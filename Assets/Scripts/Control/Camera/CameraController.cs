using UnityEngine;
using DG.Tweening;
using UnityEditor;

public class CameraController : MonoBehaviour
{
    [Header("Touch settings")]
    [SerializeField] private float _activeClickTime;
    [SerializeField] private float _touchTime = 0.15f;
    [SerializeField] private float _accelerationTime = 0.5f;

    [Header("Screen limitations")]
    [SerializeField] private float _upScreenCurrent;
    [SerializeField] private float _downScreenCurrent = 9f;
    [SerializeField] private float _offsetMoveZ = 2f;

    private Vector3 _startPosition;

    private bool _dragPanMoveActive;
    private float _startDownMousePosition;
    private float _deltaMousePosition;
    private float _startPositionZ;

    private void Awake()
    {
        _startPosition = new Vector3(transform.position.x, transform.position.y, _downScreenCurrent);
        ResetCameraAndScreenLimitations();
        EntryPoint.Instance.LevelsManager.OnChangeLevel += OnChangeLevel;
    }
    private void OnDestroy()
    {
        EntryPoint.Instance.LevelsManager.OnChangeLevel -= OnChangeLevel;
    }
    public void OnChangeLevel(int level) => ResetCameraAndScreenLimitations();
    public void ResetCameraAndScreenLimitations()
    {
        transform.position = _startPosition;

        var exit = FindObjectOfType<ExitGate>();
        _upScreenCurrent = exit.transform.position.z - _offsetMoveZ;
    }

    /*[CustomEditor(typeof(CameraController))]
    public class CameraControllerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var camera = (CameraController)target;

            if (GUILayout.Button("Reset Camera") == true && Application.isPlaying)
                camera.ResetCameraAndScreenLimitations();
        }
    }*/

    private void Update()
    {
        //if (EntryPoint.Instance.UIManager.IsNeedStopCamera) return; need code for alow camera moving

        if (Input.GetMouseButtonDown(0))
        {
            _startPositionZ = transform.position.z;
            _dragPanMoveActive = true;
            _startDownMousePosition = Input.mousePosition.y;
        }
        if (Input.GetMouseButtonUp(0))
        {
            _dragPanMoveActive = false;
            _activeClickTime = 0f;
        }


        if (_dragPanMoveActive && DragAndDropXZ.IsWorkNow == false)
        {
            _activeClickTime += Time.deltaTime;
            _deltaMousePosition = _startDownMousePosition - Input.mousePosition.y;
            float resultPos = _startPositionZ + _deltaMousePosition * 0.01f;


            if (resultPos > _upScreenCurrent)
                resultPos = _upScreenCurrent;

            if (resultPos < _downScreenCurrent)
                resultPos = _downScreenCurrent;

            if (_activeClickTime > _touchTime)
            {
                transform.DOLocalMoveZ(resultPos, _accelerationTime);
            }

        }
    }
}

