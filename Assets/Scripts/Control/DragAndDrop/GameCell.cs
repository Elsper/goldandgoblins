﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class GameCell : MonoBehaviour
{
    [SerializeField] private Vector3 _offsetPosition = new Vector3(0f, 0f, -0.5f);
    [Space()]
    [SerializeField] private Color _colorOnCanMove;
    [SerializeField] private Color _colorOnStoneLook;
    [SerializeField] private Color _colorOnCanMerge;
    [SerializeField] private Color _colorOnClear;

    private bool _stateVFX;
    [SerializeField] private GameObject _mergeVFX;

    [SerializeField] private Transform _arror;

    private SpriteRenderer _thisRenderer;
    private Transform _thisTransform;
    private float _positionY;

    private void Awake()
    {
        _thisRenderer = GetComponent<SpriteRenderer>();
        _thisTransform = GetComponent<Transform>();

        _positionY = _thisTransform.position.y;
        /*
        if (_positionY <= 0f)
        {
            Debug.LogWarning($"Position Y of Game Cell unrecommended ({_positionY}). \n Set position.y > 0f");
        }*/
    }

    public void Move(Vector3 position)
    {
        _thisTransform.position = new Vector3(
            x: position.x,
            y: _positionY,
            z: position.z) + _offsetPosition;
    }

    public void SetMergeStateTrue()
    {
        if (_stateVFX == true)
            return;

        _mergeVFX.SetActive(true);
        _stateVFX = true;

    }

    public void SetMergeStateFalse()
    {
        _mergeVFX.SetActive(false);
        _stateVFX = false;
    }

    public void SetColorOnMove() => SetColor(_colorOnCanMove);
    public void SetColorOnClear() => SetColor(_colorOnClear);
    public void SetStateForTookStone(Vector3 positionGoblin, Vector3 targetPosition)
    {
        SetColor(_colorOnStoneLook);
        //var offset = (positionGoblin + _offsetPosition - _thisTransform.position) * 0.75f;

        _arror.position = _thisTransform.position;// + offset;

        Vector3 resultLookVector = targetPosition - (_thisTransform.position - _offsetPosition - new Vector3(0, _offsetPosition.z, 0.5f));
        resultLookVector.y = 0;

        _arror.rotation = Quaternion.LookRotation(Vector3.up , resultLookVector);
        _arror.Translate(Vector3.up.normalized * 0.75f);

        _arror.gameObject.SetActive(true);
    }

    private void SetColor(Color newColor)
    {
        _thisRenderer.color = newColor;
        _arror.gameObject.SetActive(false);
    }
}
