﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class Plane : MonoBehaviour
{
    private void Awake()
    {
        var collider = GetComponent<BoxCollider>();

        collider.center = new Vector3(
            x: collider.center.x,
            y: -0.5f,
            z: collider.center.z);

        collider.size = new Vector3(
            x: collider.size.x,
            y: 0f,
            z: collider.size.z);
    }
}
