using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingMainScene : MonoBehaviour
{
    [SerializeField] private GameObject _rotationImage;

    private void Start()
    {
        StartCoroutine(AsynchronousLoad("Game"));
    }
    IEnumerator AsynchronousLoad(string scene)
    {
        yield return null;
        AsyncOperation ao = SceneManager.LoadSceneAsync(scene);

        while (!ao.isDone)
        {
            _rotationImage.transform.Rotate(Vector3.forward, -1.5f);
            yield return null;
        }
    }

    private void Update()
    {
        
    }
}
