﻿using System.Linq;
using System;
using UnityEngine;

public class MapWayPointByType : MonoBehaviour
{
    [SerializeField] private KeyValuePair<WayPointType, WayPoint>[] _wayPointsByType;

    private void Awake()
    {
        if (_wayPointsByType.Length == 0)
            Debug.LogWarning("Map ofway points is empty");

        CheckingDuplicateKeys();
    }

    public WayPoint GetAt(WayPointType wayPointType)
    {
        Predicate<KeyValuePair<WayPointType, WayPoint>> typesComparison = (x) => x.Key == wayPointType;

        if (Array.Exists(_wayPointsByType, typesComparison) == false)
            Debug.LogError($"{name} not have key: {wayPointType}");

        return Array.Find(_wayPointsByType, typesComparison).Value;
    }
    
    private void CheckingDuplicateKeys()
    {
        var originalKeys = _wayPointsByType.Select((x) => x.Key);
        var withoutDuplicateKeys = originalKeys.Distinct();

        if (originalKeys.Count() != withoutDuplicateKeys.Count())
        {
            string sourceExceptionKeys = "";
            var arrayOriginalKeys = originalKeys.ToArray();

            foreach (var key in withoutDuplicateKeys)
            {
                if (Array.FindAll(arrayOriginalKeys, (x) => x == key).Count() > 1)
                {
                    sourceExceptionKeys += $" {key}";
                }
            }

            throw new Exception($"Way Points in {name} has duplicate keys:{sourceExceptionKeys}.");
        }
    }

    private WayPoint TryGetWayPoint(WayPointType wayPointType)
    {
        try
        {
            return Array.Find(_wayPointsByType, (x) => x.Key == wayPointType).Value; // default(T)?
        }
        catch (Exception e)
        {
            if (e.Source != null)
                Debug.LogError($"{name} not have key: {wayPointType}"); //?

            throw;
        }
    }
}
