﻿using System;
using UnityEngine;

public class MoveningTracing : MonoBehaviour
{
    public Action OnStop;

    [SerializeField] private float _speedMove = 10f;
    [SerializeField] private float _speedRotate = 10f;
    [SerializeField] private float _distanceToStop = 1f;

    private Transform _currentPositionMove;
    private bool _isCanMove = false;

    public void SetPositionToMove(Transform position)
    {
        if (position == null)
            throw new Exception("Position for move is null");

        _currentPositionMove = position;
        _isCanMove = true;
    }

    public void RotateToCurrentPosition()
    {
        if (_currentPositionMove == null)
            throw new Exception("Position for sleigh rotate is not set. Use SetPositionToMove() before.");

        Quaternion lookUpToWayPoint = Quaternion.LookRotation(_currentPositionMove.position - transform.position);
        transform.rotation = lookUpToWayPoint;
    }

    private void Update()
    {
        if (_isCanMove == true)
        {
            Quaternion lookUpToWayPoint = Quaternion.LookRotation(_currentPositionMove.position - transform.position);

            transform.rotation = Quaternion.Slerp(transform.rotation, lookUpToWayPoint, _speedRotate * Time.deltaTime);
            transform.Translate(0, 0, _speedMove * Time.deltaTime);

            if (Vector3.Distance(transform.position, _currentPositionMove.position) < _distanceToStop)
            {
                _isCanMove = false;
                OnStop?.Invoke();
            }
        }
    }
}
