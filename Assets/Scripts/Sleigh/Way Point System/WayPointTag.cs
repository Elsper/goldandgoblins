﻿using UnityEngine;

public class WayPointTag : WayPoint
{
    [SerializeField] private WayPointType _type;
    private WayPoint _wayPoint;

    private void Awake()
    {
        _wayPoint = FindWayPoint();
    }

    private WayPoint FindWayPoint()
    {
        var singleMapWayPoint = FindObjectOfType<MapWayPointByType>();
        return singleMapWayPoint.GetAt(_type);
    }

    public override bool TryGetNextWayPoint(out WayPoint nextWayPoint)
    {
        nextWayPoint = _wayPoint;
        return true;
    }
}
