﻿using System.Linq;
using UnityEngine;

public class WayPointRoute : WayPoint
{
    [SerializeField] private Transform _startPosition;
    [SerializeField] private WayPoint[] _routeWayPoints;
    [SerializeField] private bool _canRestartRoute;

    private int _countWayPoints;
    private int _currentIndexRoute = -1;

    private MoveningTracing _sleigh;

    private void Awake()
    {
        _countWayPoints = _routeWayPoints.Length;
    }

    public void Spawn()
    {
        _sleigh = new InstantiaterPrefab<MoveningTracing>("Prefabs/Sleigh").Create(_startPosition.transform);

        _sleigh.SetPositionToMove(_routeWayPoints.First().transform);
        _sleigh.OnStop += SetNextWayPoint;
    }

    public void Spawn2()
    {
        var sleigh = new InstantiaterPrefab<SleighRoute>("Prefabs/Sleigh").Create(_startPosition.transform);

        var route = (WayPoint)MemberwiseClone();

        sleigh.SetStartWayPoint(route);
    }

    private void OnMouseDown()
    {
        Spawn2();
    }

    private void SetNextWayPoint()
    {
        if (TryIncreaseIndex() == true)
        {
            _sleigh.SetPositionToMove(_routeWayPoints[_currentIndexRoute].transform);
        }
        else
        {
            if (_canRestartRoute == true)
            {
                _currentIndexRoute = 0;
                _sleigh.SetPositionToMove(_routeWayPoints.First().transform);
            }
        }
    }

    private bool TryIncreaseIndex()
    {
        if (_currentIndexRoute + 1 >= _countWayPoints)
        {
            if (_canRestartRoute == false)
            {
                return false;
            }
            else
            {
                _currentIndexRoute = 0;
                return true;
            }

        }
                
        _currentIndexRoute++;
        return true;
    }

    public override bool TryGetNextWayPoint(out WayPoint nextWayPoint)
    {
        nextWayPoint = default;

        if (TryIncreaseIndex() == true)
        {
            nextWayPoint = _routeWayPoints[_currentIndexRoute];
            return true;
        }
        else
        {
            if (_canRestartRoute == true)
            {
                _currentIndexRoute = 0;

                nextWayPoint = _routeWayPoints.First();
                return true;
            }
        }

        return false;
    }
}
