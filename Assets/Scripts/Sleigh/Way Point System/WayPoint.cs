﻿using UnityEngine;

public abstract class WayPoint : MonoBehaviour
{
    public abstract bool TryGetNextWayPoint(out WayPoint nextWayPoint);
}
