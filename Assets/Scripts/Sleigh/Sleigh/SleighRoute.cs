﻿using UnityEngine;

public class SleighRoute : Sleigh
{
    private protected override Transform PositionToMove(WayPoint newWayPoint)
    {
        return newWayPoint.transform;
    }

    private protected override void ActionOnEndTracing()
    {
        // endless enjoyer
    }
}