﻿using UnityEngine;

public class SleighStarterTracing : MonoBehaviour
{
    [SerializeField] private WayPoint _starting;

    public void Move(Sleigh sleigh)
    {
        sleigh.SetStartWayPoint(_starting);
    }
}



