﻿using UnityEngine;

public class SleighDriveMine : MonoBehaviour
{
    [SerializeField] private Transform _start;
    [SerializeField] private Transform _end;

    [SerializeField] private SleighStarterTracing _starterToWayPoint;

    private Sleigh _target;

    public void SpawnNewSleigh()
    {
        _target = new InstantiaterPrefab<Sleigh>("Prefabs/Sleigh").Create(_start);
    }

    public void MoveToEndPosition()
    {
        _target.MoveTo(_end);
    }

    public void StartTracing()
    {
        _starterToWayPoint.Move(_target);
    }

}