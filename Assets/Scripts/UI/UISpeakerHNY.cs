using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UISpeakerHNY : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI _text1;
    [SerializeField] private TextMeshProUGUI _text2;


    private bool _posibleClose;
    private int _steep;

    public void GenerateScreen()
    {
        _steep = 0;
        _text1.text = "����� ��� ����������! \n\n� ������ �� ���� ������ � ���������, � ��������� ��������� ����� ������� ������!";
        _text2.text = "����� ���������� �� ��������, ��������� ������";
        _text2.gameObject.SetActive(false);


        gameObject.SetActive(true);
        DOTween.Sequence().SetDelay(2f).OnComplete(() => { _posibleClose = true; _text2.gameObject.SetActive(true); });

    }

    private void SecondPhrase()
    {
        _steep = 1;
        _posibleClose = false;
        _text1.text = "����� - ����� ������� ������� ��������. \n ��� �� ��� �� ������� ������� ��� ������ ��� ��� ����";
        _text2.text = "����� ������� � ������, ��������� ������";
        _text2.gameObject.SetActive(false);

        DOTween.Sequence().SetDelay(2f).OnComplete(() => { _posibleClose = true; _text2.gameObject.SetActive(true); });
    }

    private void Update()
    {
        if ((_posibleClose) && Input.GetMouseButtonUp(0))
        {
            if (_steep == 0)
            {
                SecondPhrase();
            }
            else
            {
                gameObject.SetActive(false);
                EntryPoint.Instance.SaveLoadManager.SLData.HNYScreenAlreadyShowed = true;
            }
        }
    }
}
