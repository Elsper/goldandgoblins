using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIRareLine : MonoBehaviour
{
    [SerializeField] private List<Sprite> _listImages;
    [SerializeField] private TextMeshProUGUI _text;
    [SerializeField] private Image _image;
    private RectTransform _rtImage;

    private void Awake()
    {
        _rtImage = _image.GetComponent<RectTransform>();
    }
    public void GenerateLineNoCards(string text)
    {
        if (_rtImage == null)
            _rtImage = _image.GetComponent<RectTransform>();
        transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
        transform.DOScale(1, 0.5f).SetDelay(0.5f);

        _text.text = text;

        if (_listImages.Count > 4)
            _image.sprite = _listImages[4];
        else
            Debug.LogError("wrong nomerImage");

        _rtImage.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _text.preferredWidth + 150);
    }

    public void GenerateLine(OneCardUI card)
    {
        if (_rtImage == null)
            _rtImage = _image.GetComponent<RectTransform>();
        _text.text = card.Card.GetRareText();

        if (_listImages.Count > (int)card.Card.Rare)
            _image.sprite = _listImages[(int)card.Card.Rare];

        _rtImage.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _text.preferredWidth + 150);
    }

    private void OnDestroy()
    {
        transform.DOKill();
    }
}
