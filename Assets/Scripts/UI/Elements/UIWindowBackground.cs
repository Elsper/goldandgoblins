using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIWindowBackground : MonoBehaviour
{
    [SerializeField] private GameObject _line1;
    [SerializeField] private GameObject _line2;
    [SerializeField] private GameObject _lineSolo;
    [SerializeField] private Button _btnClose;

    private TextMeshProUGUI _textLine1;
    private TextMeshProUGUI _textLine2;
    private TextMeshProUGUI _textLineSolo;

    public Button GetBtnClose { get { return _btnClose; } }

    private void Awake()
    {
        if (_line1!=null) _textLine1 = _line1.GetComponent<TextMeshProUGUI>();
        if (_line2 != null) _textLine2 = _line2.GetComponent<TextMeshProUGUI>();
        if (_lineSolo != null) _textLineSolo = _lineSolo.GetComponent<TextMeshProUGUI>();
    }

    public void SwichToOneLine()
    {
        _lineSolo.SetActive(true);

        _line1.SetActive(false);
        _line2.SetActive(false);
    }
    public void SwichToTwoLine()
    {
        _lineSolo.SetActive(false);

        _line1.SetActive(true);
        _line2.SetActive(true);
    }

    public void SetText(string line1, string line2)
    {
        _textLine1.text = line1;
        _textLine2.text = line2;
    }
    public void SetText(string line)
    {
        _textLineSolo.text = line;
    }


}
