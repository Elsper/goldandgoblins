using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class ChestImageByRendererTexture : MonoBehaviour
{
    [SerializeField] private RawImage _chestImage;
    private void Awake()
    {
        _chestImage = GetComponent<RawImage>();
    }
    public void SetChestTexure(Texture chestTextures)
    {
        transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
        transform.DOScale(1, 0.5f).SetDelay(0.75f);

        _chestImage.texture = chestTextures;
    }
    private void OnDestroy()
    {
        transform.DOKill();
    }
}
