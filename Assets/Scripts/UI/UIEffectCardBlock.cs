using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIEffectCardBlock : MonoBehaviour
{
    [SerializeField] private RectTransform _rtEffectBlock;
    [SerializeField] private TextMeshProUGUI _textEffectCaption;
    [SerializeField] private TextMeshProUGUI _textEffectLeft;
    [SerializeField] private TextMeshProUGUI _textEffectRight;
    [SerializeField] private GameObject _effectArrow;

    public void RegenByCard(OneCard card)
    {
        if (card.Level > 0)
        {
            _textEffectLeft.text = card.GetTextEffectCurrentLevel();
            _textEffectRight.text = card.GetTextEffectNextLevel();
            _textEffectCaption.text = card.GetCaptionEffect();

            _textEffectRight.gameObject.SetActive(true);
            _effectArrow.gameObject.SetActive(true);
        }
        else
        {
            _textEffectLeft.text = card.GetTextEffectNextLevel();
            _textEffectCaption.text = card.GetCaptionEffect();
            _textEffectRight.gameObject.SetActive(false);
            _effectArrow.gameObject.SetActive(false);
        }
        LayoutRebuilder.ForceRebuildLayoutImmediate(_rtEffectBlock);
    }
}
