using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class UIUpperPanelCurrency : MonoBehaviour
{
    [SerializeField] private TMP_Text GoldCount;
    [SerializeField] private TMP_Text PoisonCount;
    [SerializeField] private TMP_Text GemCount;

    public void Init()
    {
        EntryPoint.Instance.CurrencyManager.Coins.OnChangeCount += OnChangeMoney;
        EntryPoint.Instance.CurrencyManager.Bottles.OnChangeCount += OnChangeBottle;
        EntryPoint.Instance.CurrencyManager.Gems.OnChangeCount += OnChangeGems;

    }
    private void OnDestroy()
    {
        EntryPoint.Instance.CurrencyManager.Coins.OnChangeCount -= OnChangeMoney;
        EntryPoint.Instance.CurrencyManager.Bottles.OnChangeCount -= OnChangeBottle;
        EntryPoint.Instance.CurrencyManager.Gems.OnChangeCount -= OnChangeGems;
    }
    private void OnChangeMoney(double newValue)
    {
        GoldCount.text = SpecialFunctions.GetStringFromBigDouble(newValue);
    }
    private void OnChangeBottle(double newValue)
    {
        PoisonCount.text = SpecialFunctions.GetStringFromBigDouble(newValue);
    }
    private void OnChangeGems(double newValue)
    {
        GemCount.text = SpecialFunctions.GetStringFromBigDouble(newValue);
    }
    
    public void GemPlusClick() => EntryPoint.Instance.UIManager.MarketUI.ShowOnGems();
    public void BottlesPlusClick() => EntryPoint.Instance.UIManager.MarketUI.ShowOnBottles();
}
