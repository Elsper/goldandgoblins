using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIUpperPanelLevel : MonoBehaviour
{
    [SerializeField] private TMP_Text _textLevel;
    [SerializeField] private TMP_Text _textCountGoblins;

    public void Init()
    {
        EntryPoint.Instance.LevelsManager.OnChangeLevel += SetTextLevel;
        EntryPoint.Instance.GoblinsManager.OnChangeCountGoblins += SetCountGoblins;
    }
    private void OnDestroy()
    {
        EntryPoint.Instance.LevelsManager.OnChangeLevel -= SetTextLevel;
        EntryPoint.Instance.GoblinsManager.OnChangeCountGoblins -= SetCountGoblins;
    }

    public void SetCountGoblins(int count, int limit)
    {
        if (count == limit)
            _textCountGoblins.color = new Color(1f, 168 / 255f, 0f);
        else
            _textCountGoblins.color = Color.white;

        _textCountGoblins.text = count.ToString() + "/" + limit.ToString();
    }
    public void SetTextLevel(int lvl)
    {
        _textLevel.text = "������ " + lvl.ToString();
    }

}
