using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class DisablingAds : MonoBehaviour
{
    [Header("Connected Objects")]
    [SerializeField] private AnimatorFlightResources _gems;
    
    [Header("Game Object")]
    [SerializeField] private GameObject _panelDisablingAds;

    [Header("Amount Resources")]
    [SerializeField] private int _amountGems;
    [SerializeField] private int _amountBotteles;

    [Header("Setting")]
    [SerializeField] private float _delySecondChestOpen = 3f;

    public static UnityEvent DisablingAdsEvent = new UnityEvent();

    public void OnClickButton()
    {
        _panelDisablingAds.SetActive(true);
    }

    public void OnClickButtonExit()
    {
        _panelDisablingAds.SetActive(false);
    }

    public void OnClickButtonBuy()
    {
        StartCoroutine(ChestOpen());
        
        SpawnGems(_amountGems);

        EntryPoint.Instance.SaveLoadManager.SLData.AdsOffBuyed = true;
        DisablingAdsEvent.Invoke();
    }
    
    private void SpawnGems(int countGems)
    {
        _gems.SpawnGems(transform.position, countGems);

        EntryPoint.Instance.CurrencyManager.Gems.ChangeCount(countGems);
    }

    private void OpenChest()
    {
        EntryPoint.Instance.UIManager.ShowOpenChestScreenUI(ChestType.Gold, _amountBotteles);
        
        _panelDisablingAds.SetActive(false);
    }

    private IEnumerator ChestOpen()
    {
        yield return new WaitForSeconds(_delySecondChestOpen);
        
        OpenChest();
    }
}
