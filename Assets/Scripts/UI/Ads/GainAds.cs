using UnityEngine;

public class GainAds : MonoBehaviour
{
    [Header("Game object")]
    [SerializeField] private GameObject _gainAds;
    [SerializeField] private GameObject _buttonDisablingAds;
    [SerializeField] private GameObject _panelAdsWasShowScreen;
    [SerializeField] private AnimatorFlightResources _golds;

    [Header("Setting")]
    [SerializeField] private float _amountGolds;

    private int countGolds;
    
    private void Start()
    {
        DisablingAds.DisablingAdsEvent.AddListener(OnDestroy);
    }

    public void OnClickGain()
    {
        _gainAds.SetActive(true);
    }
    
    public void OnClickButtonAds()
    {
        _panelAdsWasShowScreen.SetActive(true);
        _gainAds.SetActive(false);
        
        SpawnGold(_amountGolds);
    }

    public void OnClickButtonClose()
    {
        _gainAds.SetActive(false);
    }
    
    private void SpawnGold(float countGolds)
    {
        _golds.SpawnStackGold(transform.position);
        
        EntryPoint.Instance.CurrencyManager.Coins.ChangeCount(countGolds);
    }

    private void OnDestroy()
    {
        Destroy(_buttonDisablingAds);
    }
}
