using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdsWasShowScreen : MonoBehaviour
{
    [SerializeField] private Button _buttonConfirm;

    private Action _currentCallback;

    private void Awake()
    {
        _buttonConfirm.onClick.AddListener(OnClickConfirm);
    }

    private void OnDestroy()
    {
        _buttonConfirm.onClick.RemoveAllListeners();
    }

    public void Callback(Action callback) => _currentCallback = callback;
    public AdsWasShowScreen ShowAds()
    {
        gameObject.SetActive(true);
        return this;
    }

    public void OnClickConfirm()
    {
        gameObject.SetActive(false);
        _currentCallback?.Invoke();
    }
}
