using TMPro;
using UnityEngine;

public class RotationCounter : MonoBehaviour
{
    [Header("Amount Of Rotation")]
    [SerializeField] private TMP_Text _textAmount;
    [SerializeField] private int _clickAmount;
    
    public int ClickAmount
    {
        get { return _clickAmount; }
        set { _clickAmount = value; }
    }

    [Header("Next Time Rotation")]
    [SerializeField] private TMP_Text _timerText;
    [SerializeField] private float _timeStart;
    [SerializeField] private float _timeCurrent;

    public float TimeCurrent
    {
        get { return _timeCurrent; }
        set { _timeCurrent = value; }
    }

    private bool timer = false;

    [Header("Game Object")]
    [SerializeField] private GameObject _buttonRotate;
    [SerializeField] private GameObject _buttonNotReady;
    [SerializeField] private GameObject _textTotalAmount;
    [SerializeField] private GameObject _textNotTotalAmount;
    [SerializeField] private GameObject _textNextTimeRotation;

    [Header("Rotation Panels")]
    [SerializeField] private GameObject _panelFreeDailySpin;
    [SerializeField] private GameObject _panelRotationForAds;

    private void Start()
    {
        _timeCurrent = _timeStart;
        _timerText.text = _timeCurrent.ToString();
    }

    private void Update()
    {
        if (timer == true)
        {
            _timeCurrent -= Time.deltaTime;
            _timerText.text = TimeConv.FormatForWheel(_timeCurrent);
        }

        if (_timeCurrent < 0)
        {
            _timeCurrent = 0;
            if (_clickAmount > 0)
            {
                _buttonNotReady.SetActive(false);
                _buttonRotate.SetActive(true);
            }
            _timeCurrent = _timeStart;
            timer = !timer;
        }

        if (_clickAmount == 5)
        {
            _textTotalAmount.SetActive(true);
            _textNotTotalAmount.SetActive(false);
            _textNextTimeRotation.SetActive(false);
        }
        if (_clickAmount < 5)
        {
            _textTotalAmount.SetActive(false);
            _textNotTotalAmount.SetActive(true);
            _textNextTimeRotation.SetActive(true);
        }
    }

    public void ButtonTimer()
    {
        timer = !timer;
    }

    public void OnButtonFreeDaily()
    {
        _panelFreeDailySpin.SetActive(false);
        _panelRotationForAds.SetActive(true);
    }

    public void OnButtonClick()
    {
        _clickAmount--;
        _buttonRotate.SetActive(false);
        _buttonNotReady.SetActive(true);

        if (_clickAmount <= 0)
        {
            _clickAmount = 0;
            timer = false;
            _buttonRotate.SetActive(false);
            _buttonNotReady.SetActive(true);
        }
        
        _textAmount.text = _clickAmount.ToString() + "/5";
    }
}
