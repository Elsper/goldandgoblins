using UnityEngine;

public class PanelWheelFortune : MonoBehaviour
{
    [Header("Game Object")]
    [SerializeField] private GameObject _panelWheelFortune;

    public void OnClickButtonOpen()
    {
        _panelWheelFortune.SetActive(true);
    }

    public void OnClickButtonClose()
    {
        _panelWheelFortune.SetActive(false);
    }
}
