using System.Collections;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class WheelFortune : MonoBehaviour
{
    [Header("Prefab")]
    [SerializeField] private AnimatorFlightResources _resources;
    
    [Header("Text")]
    [SerializeField] private TMP_Text _winningText;

    [Header("Amount of resources in the sector")]
    [SerializeField] private string[] _prizeText;

    private int numberOfTurns;
    private int whatWeWin;
    private int winAmount;

    private float speed; // вращения и затухания
    
    private bool _canWeTurn; // можно ли вращать колесо

    private void Start()
    {
        _canWeTurn = true;
    }

    public void OnButtonClick()
    {
        StartCoroutine(TurnTheWheel());
    }

    private IEnumerator TurnTheWheel()
    {
        _canWeTurn = false;

        numberOfTurns = Random.Range(30, 50);
        
        speed = 0.01f; // чем больше значение тем медленние

        for (int i = 0; i < numberOfTurns; i++)
        {
            EffectsAudioManager.PlaySoundWheelClick();
            transform.Rotate(0, 0 , 22.5f);

            // если кол. оборотов > 0.5(пройдено больше 50%), то замдляем его
            if (i > Mathf.RoundToInt(numberOfTurns * 0.5f))
            {
                speed = 0.03f;
            }
            if (i > Mathf.RoundToInt(numberOfTurns * 0.7f))
            {
                speed = 0.06f;
            }
            if (i > Mathf.RoundToInt(numberOfTurns * 0.8f))
            {
                speed = 0.08f;
            }
            if (i > Mathf.RoundToInt(numberOfTurns * 0.9f))
            {
                speed = 0.1f;
            }

            yield return new WaitForSeconds(speed);
        }

        // если угол не попадает на нужное деление, то мы его докручиваем
        // 45, потому что у нас 8 делений на колесе (360/8 = 45)
        if (Mathf.RoundToInt(transform.eulerAngles.z) % 45 != 0)
        {
            transform.Rotate(0, 0, 22.5f); // доп. один оборот
        }

        whatWeWin = Mathf.RoundToInt(transform.eulerAngles.z);
        
        switch (whatWeWin)
        {
            // угол деления (сектора)
            case 0:
                _winningText.text = _prizeText[0];
                winAmount = int.Parse(_prizeText[0]);
                SpawnElixirs(winAmount);
                break;
            case 45:
                _winningText.text = _prizeText[1];
                winAmount = int.Parse(_prizeText[1]);
                SpawnGoblins(winAmount);
                break;
            case 90:
                _winningText.text = _prizeText[2];
                winAmount = int.Parse(_prizeText[2]);
                SpawnElixirs(winAmount);
                break;
            case 135:
                _winningText.text = _prizeText[3];
                winAmount = int.Parse(_prizeText[3]);
                SpawnGems(winAmount);
                break;
            case 180:
                _winningText.text = _prizeText[4];
                winAmount = int.Parse(_prizeText[4]);
                SpawnElixirs(winAmount);
                break;
            case 225:
                _winningText.text = _prizeText[5];
                winAmount = int.Parse(_prizeText[5]);
                SpawnGoblins(winAmount);
                break;
            case 270:
                _winningText.text = _prizeText[6];
                winAmount = int.Parse(_prizeText[6]);
                SpawnElixirs(winAmount);
                break;
            case 315:
                _winningText.text = _prizeText[7];
                winAmount = int.Parse(_prizeText[7]);
                SpawnGems(winAmount);
                break;
        }

        _canWeTurn = true;
        EffectsAudioManager.PlaySoundWheelEnd();
    }

    private void SpawnGems(int countGems)
    {
        _resources.SpawnGems(transform.position, countGems);

        EntryPoint.Instance.CurrencyManager.Gems.ChangeCount(countGems);
    }

    private void SpawnElixirs(int countElixirs)
    {
        _resources.SpawnElixirs(transform.position, countElixirs);
        
        EntryPoint.Instance.CurrencyManager.Bottles.ChangeCount(countElixirs);
    }

    private void SpawnGoblins(int levelGoblins)
    {
        EntryPoint.Instance.UIManager.UIBottomPanel.Fire(true, levelGoblins);
    }
}
