﻿using UnityEngine;

public class FactoryGameCell
{
    private GameCell _cellPrefab;
    
    public FactoryGameCell()
    {
        _cellPrefab = Resources.Load<GameCell>("Prefabs/GameCell");
    }

    public GameCell Create()
    {
        return GameObject.Instantiate(_cellPrefab);
    }
}
