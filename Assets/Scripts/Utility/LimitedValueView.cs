﻿using UnityEngine;
using TMPro;

[RequireComponent(typeof(LimitedValue))] // forced measure. direct dependence and MonoBehaviuor business class.
public class LimitedValueView :MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _textComponent;
    [SerializeField] private Animation _animationWhenTryOverLimit;
    [SerializeField] private LimitedValue _value;

    [SerializeField] private Color _basicColor = new Color(1f, 1f, 1f, 1f);
    [SerializeField] private Color _limitColor = new Color(1f, 1f, 0f, 1f);

    private void Awake()
    {
        if (_animationWhenTryOverLimit == null)
            _animationWhenTryOverLimit = GetComponent<Animation>();

        if (_value == null)
            _value = GetComponent<LimitedValue>();

        if (_textComponent == null)
            _textComponent = GetComponentInChildren<TextMeshProUGUI>();
        if (_textComponent == null)
            if (TryGetComponent(out _textComponent) == false)
                throw new System.Exception($"TextMeshProUGUI not found in {name} Game Object or in his childrens.");
    }

    private void OnEnable()
    {
        _value.OnChange += UpdateText;
        _value.OnReachLimit += SetLimitColor;
        _value.OnTryOverLimit += StartAnimation;
    }
    private void OnDisable()
    {
        _value.OnChange -= UpdateText;
        _value.OnReachLimit -= SetLimitColor;
        _value.OnTryOverLimit -= StartAnimation;
    }

    private void UpdateText(int currentValue, int limitValue)
    {
        _textComponent.text = $"{currentValue}/{limitValue}";
    }
    private void SetLimitColor(bool stateLimit)
    {
        _textComponent.color = _basicColor;

        if (stateLimit == true)
            _textComponent.color = _limitColor;
    }
    private void StartAnimation()
    {
        _animationWhenTryOverLimit.Stop();
        _animationWhenTryOverLimit.Play();
    }
}