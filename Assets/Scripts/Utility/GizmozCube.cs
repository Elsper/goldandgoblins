﻿using UnityEngine;

public class GizmozCube : MonoBehaviour
{
    [SerializeField] private Color _color = new Color(255f, 0f, 0f, 1f);
    [SerializeField] private Vector3 _size = new Vector3(1f, 0f, 1f);

    private void OnDrawGizmos()
    {
        Gizmos.color = _color;
        Gizmos.DrawCube(
            center: transform.position,
            size: Vector3.Scale(_size, transform.localScale));
    }
}