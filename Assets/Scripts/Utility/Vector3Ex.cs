﻿using UnityEngine;

public static class Vector3Ex
{
    public static Vector3 RountToIntXZ(this Vector3 vector)
    {
        var result = new Vector3(
            x: Mathf.RoundToInt(vector.x),
            y: vector.y,
            z: Mathf.RoundToInt(vector.z));

        return result;
    }
}
