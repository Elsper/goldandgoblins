﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using Newtonsoft.Json;

public class SaveLoadLevelState
{
    public class SLDestroyableObject
    {
        public double PerSecConsumeDurability;
        public double CurrentDurability;
        public Vector2Int Pos;

        [JsonConstructor]
        public SLDestroyableObject() { }
        public SLDestroyableObject(DestroyableObject baseObject, Vector2Int pos)
        {
            PerSecConsumeDurability = baseObject.PerSecConsumeDurability;
            CurrentDurability = baseObject.CurrentDurability;
            Pos = pos;
        }
    }
    public class SLPIckableObject
    {
        public PicableType PicableType;
        public double Count;
        public Vector2Int Pos;

        [JsonConstructor]
        public SLPIckableObject() { }
        public SLPIckableObject(PickableObject baseObject, Vector2Int pos)
        {
            PicableType = baseObject.PicableType;
            Count = baseObject.Count;
            Pos = pos;
        }
    }
    public class SLBarrelObject
    {
        public int GoblinLevel;
        public Vector2Int Pos;

        [JsonConstructor]
        public SLBarrelObject() { }
        public SLBarrelObject(BarrelScript baseObject, Vector2Int pos)
        {
            GoblinLevel = baseObject.GoblinLevel;
            Pos = pos;
        }
    }

    public List<SLDestroyableObject> SLDestroyableObjects = new List<SLDestroyableObject>();
    public List<SLPIckableObject> SLPickableObjects = new List<SLPIckableObject>();
    public List<SLBarrelObject> SLBarrelsObjects = new List<SLBarrelObject>();
    public SaveLoadGoblinsState SLGoblinsState = new SaveLoadGoblinsState();
    public SaveLoadMineState SLMineState = new SaveLoadMineState();

    public int ForgeLevel;
    public double CountCoins;
   
    public SaveLoadLevelState()
    {
        SLDestroyableObjects = new List<SLDestroyableObject>();
        SLPickableObjects = new List<SLPIckableObject>();
        SLBarrelsObjects = new List<SLBarrelObject>();
        ForgeLevel = 1;

    }

}

public class Level : MonoBehaviour
{
    [SerializeField] private int _levelForMines = 3;

    private Dictionary<Vector2Int, DestroyableObject> _allDestroyableObjects = new Dictionary<Vector2Int, DestroyableObject>();
    private Dictionary<Vector2Int, PickableObject> _allPickableObjects = new Dictionary<Vector2Int, PickableObject>();
    private Dictionary<Vector2Int, BarrelScript> _allBarrelsObjects = new Dictionary<Vector2Int, BarrelScript>();
    private Forge _currentForge;

    private double _countCoinsThisLevel;
    public SaveLoadLevelState SLLevelState = new SaveLoadLevelState();

    public int LevelForMines { get => _levelForMines; set => _levelForMines = value; }
    public double CountCoinsThisLevel { get => _countCoinsThisLevel; set => _countCoinsThisLevel = value; }

    public void LoadCountCoins(double count)
    {
        _countCoinsThisLevel = count;
        EntryPoint.Instance.CurrencyManager.Coins.SetCount(_countCoinsThisLevel);
    }


    public bool IsPlaceFreeByPos(Vector2Int pos)
    {
        if (_allDestroyableObjects.ContainsKey(pos)) return false;
        if (_allPickableObjects.ContainsKey(pos)) return false;
        if (_allBarrelsObjects.ContainsKey(pos)) return false;
        return !EntryPoint.Instance.GoblinsManager.IsGoblinExistOnScene(pos);
    }
    public bool IsPlaceFreeByPos(int x, int y)
    {
        Vector2Int pos = new Vector2Int(x, y);
        return IsPlaceFreeByPos(pos);
    }

    //Костылим в спешке
    public SaveLoadLevelState GetSaveData()
    {
        if (_currentForge == null)
            return null;

        SLLevelState = new SaveLoadLevelState();

        SLLevelState.CountCoins = CountCoinsThisLevel;
        SLLevelState.ForgeLevel = _currentForge.Level;
        SLLevelState.SLGoblinsState = EntryPoint.Instance.GoblinsManager.SaveLoadGoblinsState;
        SLLevelState.SLMineState = EntryPoint.Instance.MineSidesManager.SaveLoadMineState;

        foreach (var item in _allDestroyableObjects.Keys)
            SLLevelState.SLDestroyableObjects.Add(new SaveLoadLevelState.SLDestroyableObject(_allDestroyableObjects[item], item));
        foreach (var item in _allPickableObjects.Keys)
            SLLevelState.SLPickableObjects.Add(new SaveLoadLevelState.SLPIckableObject(_allPickableObjects[item], item));
        foreach (var item in _allBarrelsObjects.Keys)
            SLLevelState.SLBarrelsObjects.Add(new SaveLoadLevelState.SLBarrelObject(_allBarrelsObjects[item], item));

        return SLLevelState;
    }

    public void LoadLevelFromSaveData()
    {
        if (SLLevelState == null)
            return;

        _currentForge.Level = SLLevelState.ForgeLevel;
        LoadCountCoins(SLLevelState.CountCoins);

        //УДаление того, что не загрузилось
        //Сначала собираем весь список объектов, потом выкинем из него то, что загрузилось, потом пробежимся и удалим оставшееся.
        List<Vector2Int> PosDestroyableObjectsShouldBeDelete = new List<Vector2Int>();
        foreach (var item in _allDestroyableObjects)
            PosDestroyableObjectsShouldBeDelete.Add(item.Key);

        if (SLLevelState.SLDestroyableObjects != null)
            foreach (var item in SLLevelState.SLDestroyableObjects)
            {
                _allDestroyableObjects[item.Pos].CurrentDurability = item.CurrentDurability;
                PosDestroyableObjectsShouldBeDelete.Remove(item.Pos);
            }

        foreach (var item in PosDestroyableObjectsShouldBeDelete)
            _allDestroyableObjects[item].CurrentDurability = 0;

        if (SLLevelState.SLPickableObjects != null)
            foreach (var item in SLLevelState.SLPickableObjects)
            {
                switch (item.PicableType)
                {
                    case PicableType.coins:
                        EntryPoint.Instance.PicableResourcesManager.MakeGoldOnSceneWithCount(item.Count, item.Pos);
                        break;
                    case PicableType.bottles:
                        EntryPoint.Instance.PicableResourcesManager.MakeSoftCurrencyOnScene(item.Count, item.Pos);
                        break;
                    case PicableType.gems:
                        EntryPoint.Instance.PicableResourcesManager.MakeHardCurrencyOnScene(item.Count, item.Pos);
                        break;
                }
            }

        if (SLLevelState.SLBarrelsObjects != null)
            foreach (var item in SLLevelState.SLBarrelsObjects)
            {
                EntryPoint.Instance.UIManager.UIBottomPanel.MakeFastBarrel(item.Pos, item.GoblinLevel);
            }


        EntryPoint.Instance.GoblinsManager.SaveLoadGoblinsState = SLLevelState.SLGoblinsState;
        EntryPoint.Instance.MineSidesManager.SaveLoadMineState = SLLevelState.SLMineState;
    }

    #region Forge
    public void AddForgeFromScene(Forge forge) => _currentForge = forge;
    #endregion

    #region PickableObjects
    public void AddPickableObjectFromScene(PickableObject pickableObject, Vector2Int pos)
    {
        if (!_allPickableObjects.ContainsKey(pos))
        {
            _allPickableObjects[pos] = pickableObject;
        }
    }
    public PickableObject getPickableObjectByPos(Vector2Int pos)
    {
        if (_allPickableObjects.ContainsKey(pos))
            return _allPickableObjects[pos];
        else
            return null;
    }
    public void DestroyPickableFromScene(Vector2Int pos)
    {
        _allPickableObjects.Remove(pos);
    }
    #endregion

    #region DestroyableObjects
    public DestroyableObject getDestroyableObjectByPos(Vector2Int pos)
    {
        if (_allDestroyableObjects.ContainsKey(pos))
            return _allDestroyableObjects[pos];
        else
            return null;
    }
    public void AddDestroyableObjectFromScene(DestroyableObject destroyableObject, Vector2Int pos)
    {
        if (!_allDestroyableObjects.ContainsKey(pos))
        {
            _allDestroyableObjects[pos] = destroyableObject;
        }
    }
    public void RemoveDestroyableObjectFromScene(Vector2Int pos)
    {
        _allDestroyableObjects.Remove(pos);
    }
    #endregion

    #region Barrels
    public void AddBarrelObjectFromScene(BarrelScript pickableObject, Vector2Int pos)
    {
        if (!_allBarrelsObjects.ContainsKey(pos))
        {
            _allBarrelsObjects[pos] = pickableObject;
        }
    }
    public void RemoveBarrelObjectFromScene(Vector2Int pos)
    {
        _allBarrelsObjects.Remove(pos);
    }
    #endregion
}
