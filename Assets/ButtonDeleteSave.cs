using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ButtonDeleteSave : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI _bottomText;
    private Button _button;
    // Update is called once per frame
    void Update()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(() =>
            {
                EntryPoint.Instance.SaveLoadManager.DeleteSave();
                if (_bottomText != null)
                    _bottomText.text = $"Сохранение стерто. Перезапустите игру";
            });
    }
    private void OnDestroy()
    {
        _button.onClick.RemoveAllListeners();
    }
}
