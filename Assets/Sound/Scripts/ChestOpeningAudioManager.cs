using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestOpeningAudioManager : MonoBehaviour
{
    public static ChestOpeningAudioManager instance = null;

    [Header("Main Game Sound")]
    [SerializeField] private AudioSource _swooshSource;
    [SerializeField] private AudioSource _openingChestSource;
    [SerializeField] private AudioSource _finalAwardSource;
    [SerializeField] private AudioSource _viewing�hestSource;
    [SerializeField] private AudioSource _unlocking�ardSource;

    [SerializeField] private AudioClip _swooshSound;
    [SerializeField] private AudioClip _openingChestSound;
    [SerializeField] private AudioClip _finalAwardSound;
    [SerializeField] private AudioClip _viewing�hestdSound;
    [SerializeField] private AudioClip _unlocking�ardSound;

    [Header("Main New Years Event Sound")]
    [SerializeField] private AudioClip _eventSwooshSound;
    [SerializeField] private AudioClip _eventFinalAwardSound;
    [SerializeField] private AudioClip _eventViewing�hestdSound;
    [SerializeField] private AudioClip _eventUnlocking�ardSound;

    [Header("Static")]
    static private AudioSource s_swooshSource;
    static private AudioSource s_openingChestSource;
    static private AudioSource s_finalAwardSource;
    static private AudioSource s_viewing�hestSource;
    static private AudioSource s_unlocking�ardSource;

    static private AudioClip s_swooshSound;
    static private AudioClip s_openingChestSound;
    static private AudioClip s_finalAwardSound;
    static private AudioClip s_viewing�hestdSound;
    static private AudioClip s_unlocking�ardSound;

    private void Start()
    {
        InitializeAudioSource();
        InitializeSoundsMainGame();
        DontDestroyOnLoad(gameObject);
    }

    public static void PlaySound�hestDrop()
    {
        s_swooshSource.PlayOneShot(s_swooshSound);
    }
    public static void PlaySoundOpeningChest()
    {
        s_openingChestSource.PlayOneShot(s_openingChestSound);
    }
    public static void PlaySoundViewing�hest()
    {
        s_viewing�hestSource.PlayOneShot(s_viewing�hestdSound);
    }

    public static void PlaySoundFinalAward()
    {
        s_finalAwardSource.PlayOneShot(s_finalAwardSound);
    }

    public static void PlaySoundUnlocking�ard()
    {
        s_unlocking�ardSource.PlayOneShot(s_unlocking�ardSound);
    }


    public void InitializeSoundsMainGame()
    {
        s_swooshSound = _swooshSound;
        s_openingChestSound = _openingChestSound;
        s_finalAwardSound = _finalAwardSound;
        s_viewing�hestdSound = _viewing�hestdSound;
        s_unlocking�ardSound = _unlocking�ardSound;
    }

    public void InitializationSoundsNewYearsEvent()
    {
        s_swooshSound = _eventSwooshSound;
        s_finalAwardSound = _eventFinalAwardSound;
        s_viewing�hestdSound = _eventViewing�hestdSound;
        s_unlocking�ardSound = _eventUnlocking�ardSound;
    }

    private void InitializeAudioSource()
    {
        s_swooshSource = _swooshSource;
        s_finalAwardSource = _finalAwardSource;
        s_viewing�hestSource = _viewing�hestSource;
        s_unlocking�ardSource = _unlocking�ardSource;
        s_openingChestSource = _openingChestSource;
    }
}
