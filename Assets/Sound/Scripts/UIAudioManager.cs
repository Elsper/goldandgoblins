using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAudioManager : MonoBehaviour
{
    public static UIAudioManager instance = null;

    [Header("Main Game Sound")]
    [SerializeField] private  AudioSource _clickSource;
    [SerializeField] private  AudioSource _swooshSource;

    [SerializeField] private  AudioClip _clickSound;
    [SerializeField] private  AudioClip[] _swooshSound;
    [Header("Main New Years Event Sound")]
    [SerializeField] private AudioClip _eventClickSound;
    [SerializeField] private AudioClip[] _eventSwooshSound;

    [Header("Static")]
    static private int s_indexOpenSwoosh;
    static private int s_indexCloseSwoosh;
    static private int s_indexSoundHelper;

    static private AudioSource s_clickSource;
    static private AudioSource s_swooshSource;

    static private AudioClip s_clickSound;
    static private AudioClip[] s_swooshSound;

    private void Start()
    {
        s_indexOpenSwoosh = 0;
        s_indexCloseSwoosh = 1;
        s_indexSoundHelper = 0;

        DontDestroyOnLoad(gameObject);
        InitializeAudioSource();
        InitializeSoundsMainGame();

    }

    public static void PlaySoundIconsOpen()
    {
        s_clickSource.PlayOneShot(s_clickSound);
        s_swooshSource.PlayOneShot(s_swooshSound[s_indexOpenSwoosh]);
    }

    public static void PlaySoundIconsClose()
    {
        s_clickSource.PlayOneShot(s_clickSound);
        s_swooshSource.PlayOneShot(s_swooshSound[s_indexCloseSwoosh]);
    }

    public static void PlaySoundHelper()
    {
        s_swooshSource.PlayOneShot(s_swooshSound[s_indexSoundHelper]);
        s_indexSoundHelper++;
        if (s_indexSoundHelper == s_swooshSound.Length)
            s_indexSoundHelper = 0;
    }
    public static void PlaySoundCardOpen()
    {
        s_swooshSource.PlayOneShot(s_swooshSound[s_indexOpenSwoosh]);
    }

    public void InitializeSoundsMainGame()
    {
        s_clickSound = _clickSound;
        s_swooshSound = _swooshSound;
    }


    public void InitializationSoundsNewYearsEvent()
    {
        s_clickSound = _eventClickSound;
        s_swooshSound = _eventSwooshSound;
    }

    private void InitializeAudioSource()
    {
        s_clickSource = _clickSource;
        s_swooshSource = _swooshSource;
    }
}
