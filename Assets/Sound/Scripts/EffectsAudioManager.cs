using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsAudioManager : MonoBehaviour
{
    public static EffectsAudioManager instance = null;
    [Header("Main Game Sound")]
    [SerializeField] private AudioSource _mortarShotSource;
    [SerializeField] private AudioSource _landingBarrelSource;
    [SerializeField] private AudioSource _destructionBarrelSource;
    [SerializeField] private AudioSource _goldSource;
    [SerializeField] private AudioSource _elixirSource;
    [SerializeField] private AudioSource _gemSource;
    [SerializeField] private AudioSource _clickSource;
    [SerializeField] private AudioSource _brokenStoneSource;
    [SerializeField] private AudioSource _clearingRubbleSource;
    [SerializeField] private AudioSource _exitCelebrationSource;
    [SerializeField] private AudioSource _exitOpenDoorsSource;
    [SerializeField] private AudioSource _upgradeSource;
    [SerializeField] private AudioSource _wheelClickSource;
    [SerializeField] private AudioSource _wheelEndSource;

    [SerializeField] private AudioClip _mortarShotSound;
    [SerializeField] private AudioClip _landingBarrelSound;
    [SerializeField] private AudioClip[] _destructionBarrelSound;
    [SerializeField] private AudioClip _goldSound;
    [SerializeField] private AudioClip _elixirSound;
    [SerializeField] private AudioClip _gemSound;
    [SerializeField] private AudioClip _clickSound;
    [SerializeField] private AudioClip _brokenStoneSound;
    [SerializeField] private AudioClip _clearingRubbleSound;
    [SerializeField] private AudioClip _exitCelebrationSound;
    [SerializeField] private AudioClip _exitOpenDoorsSound;
    [SerializeField] private AudioClip _upgradeSound;
    [SerializeField] private AudioClip _wheelClickSound;
    [SerializeField] private AudioClip _wheelEndSound;

    [Header("Main New Years Event Sound")]
    [SerializeField] private AudioClip _eventLandingBarrelSound;
    [SerializeField] private AudioClip[] _eventDestructionBarrelSound;
    [SerializeField] private AudioClip _eventElixirSound;
    [SerializeField] private AudioClip _eventClickSound;
    [SerializeField] private AudioClip _eventBrokenStoneSound;
    [SerializeField] private AudioClip _eventClearingRubbleSound;
    [SerializeField] private AudioClip _eventExitCelebrationSound;
    [SerializeField] private AudioClip _eventExitOpenDoorsSound;
    [SerializeField] private AudioClip _eventUpgradeSound;
    [SerializeField] private AudioClip _eventWheelClickSound;
    [SerializeField] private AudioClip _eventWheelEndSound;

    [Header("Static")]
    static private AudioSource s_mortarShotSource;
    static private AudioSource s_landingBarrelSource;
    static private AudioSource s_destructionBarrelSource;
    static private AudioSource s_goldSource;
    static private AudioSource s_elixirSource;
    static private AudioSource s_gemSource;
    static private AudioSource s_clickSource;
    static private AudioSource s_brokenStoneSource;
    static private AudioSource s_clearingRubbleSource;
    static private AudioSource s_exitCelebrationSource;
    static private AudioSource s_exitOpenDoorsSource;
    static private AudioSource s_upgradeSource;
    static private AudioSource s_wheelClickSource;
    static private AudioSource s_wheelEndSource;

    static private AudioClip s_mortarShotSound;
    static private AudioClip s_landingBarrelSound;
    static private AudioClip[] s_destructionBarrelSound;
    static private AudioClip s_goldSound;
    static private AudioClip s_elixirSound;
    static private AudioClip s_gemSound;
    static private AudioClip s_clickSound;
    static private AudioClip s_brokenStoneSound;
    static private AudioClip s_clearingRubbleSound;
    static private AudioClip s_exitCelebrationSound;
    static private AudioClip s_exitOpenDoorsSound;
    static private AudioClip s_upgradeSound;
    static private AudioClip s_eventWheelClickSound;
    static private AudioClip s_eventWheelEndSound;

    private static int s_indexSoundBarrel;

    private void Awake()
    {
        s_indexSoundBarrel = 0;
        InitializeAudioSource();
        InitializeSoundsMainGame();
        DontDestroyOnLoad(gameObject);
    }

    public static void PlaySoundMortarShot()
    {
        if (s_mortarShotSource!=null)
        s_mortarShotSource.PlayOneShot(s_mortarShotSound);
    }

    public static void PlaySoundLandingBarrel()
    {
        if (s_landingBarrelSource != null)
        s_landingBarrelSource.PlayOneShot(s_landingBarrelSound);
    }

    public static void PlaySoundDestructionBarrel()
    {
        s_destructionBarrelSource.PlayOneShot(s_destructionBarrelSound[s_indexSoundBarrel]);
        s_indexSoundBarrel++;
        if (s_indexSoundBarrel == s_destructionBarrelSound.Length)
            s_indexSoundBarrel = 0;
    }

    public static void PlaySoundGold()
    {
        s_goldSource.PlayOneShot(s_goldSound);
    }

    public static void PlaySoundElixir()
    {
        s_elixirSource.PlayOneShot(s_elixirSound);
    }

    public static void PlaySoundGem()
    {
        s_gemSource.PlayOneShot(s_gemSound);
    }

    public static void PlaySoundUpLevel()
    {
        s_clickSource.PlayOneShot(s_clickSound);
    }

    public static void PlaySoundBrokenStone()
    {
        s_brokenStoneSource.PlayOneShot(s_brokenStoneSound);
    }

    public static void PlaySoundClearingRubble()
    {
        s_clearingRubbleSource.PlayOneShot(s_clearingRubbleSound);
    }


    public static void PlaySoundExitCelebration()
    {
        s_exitCelebrationSource.PlayOneShot(s_exitCelebrationSound);
    }

    public static void PlaySoundExitOpenDoors()
    {
        s_exitOpenDoorsSource.PlayOneShot(s_exitOpenDoorsSound);
    }

    public static void PlaySoundUpgrade()
    {
        s_upgradeSource.PlayOneShot(s_upgradeSound);
    }

    public static void PlaySoundWheelClick()
    {
        s_wheelClickSource.PlayOneShot(s_eventWheelClickSound);
    }

    public static void PlaySoundWheelEnd()
    {
        s_wheelEndSource.PlayOneShot(s_eventWheelEndSound);
    }

    public void InitializeSoundsMainGame()
    {
        s_mortarShotSound = _mortarShotSound;
        s_landingBarrelSound = _landingBarrelSound;
        s_destructionBarrelSound = _destructionBarrelSound;
        s_goldSound = _goldSound;
        s_elixirSound = _elixirSound;
        s_gemSound = _gemSound;
        s_clickSound = _clickSound;
        s_brokenStoneSound = _brokenStoneSound;
        s_clearingRubbleSound = _clearingRubbleSound;
        s_exitCelebrationSound = _exitCelebrationSound;
        s_exitOpenDoorsSound = _exitOpenDoorsSound;
        s_upgradeSound = _upgradeSound;
        s_eventWheelClickSound = _wheelClickSound;
        s_eventWheelEndSound = _wheelEndSound;
    }

    public void InitializationSoundsNewYearsEvent()
    {
        s_landingBarrelSound = _eventLandingBarrelSound;
        s_destructionBarrelSound = _eventDestructionBarrelSound;
        s_elixirSound = _eventElixirSound;
        s_clickSound = _eventClickSound;
        s_brokenStoneSound = _eventBrokenStoneSound;
        s_clearingRubbleSound = _eventClearingRubbleSound;
        s_exitCelebrationSound = _eventExitCelebrationSound;
        s_exitOpenDoorsSound = _eventExitOpenDoorsSound;
        s_upgradeSound = _eventUpgradeSound;
        s_eventWheelClickSound = _eventWheelClickSound;
        s_eventWheelEndSound = _eventWheelEndSound;

    }

    private void InitializeAudioSource()
    {
        s_mortarShotSource = _mortarShotSource;
        s_landingBarrelSource = _landingBarrelSource;
        s_destructionBarrelSource = _destructionBarrelSource;
        s_goldSource = _goldSource;
        s_elixirSource = _elixirSource;
        s_gemSource = _gemSource;
        s_clickSource = _clickSource;
        s_brokenStoneSource = _brokenStoneSource;
        s_clearingRubbleSource = _clearingRubbleSource;
        s_exitCelebrationSource = _exitCelebrationSource;
        s_exitOpenDoorsSource = _exitOpenDoorsSource;
        s_upgradeSource = _upgradeSource;
        s_wheelClickSource = _wheelClickSource;
        s_wheelEndSource = _wheelEndSource;
    }
}
