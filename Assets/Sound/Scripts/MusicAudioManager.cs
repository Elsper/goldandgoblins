using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicAudioManager : MonoBehaviour
{
    public static MusicAudioManager instance = null;
    [Header("Main Game Sound")]
    [SerializeField] private AudioSource _musicMainSource;
    [SerializeField] private AudioSource _musicRewardflowSource;

    [SerializeField] private AudioClip _musicMainSound;
    [SerializeField] private AudioClip _musicRewardflowSound;

    [Header("Main New Years Event Sound")]
    [SerializeField] private AudioClip _eventMusicMainSound;
    [SerializeField] private AudioClip _eventMusicRewardflowSound;

    [Header("Static")]
    private static AudioSource s_musicMainSource;
    private static AudioSource s_musicRewardflowSource;
    private static AudioClip s_musicMainSound;
    private static AudioClip s_musicRewardflowSound;

    private void Start()
    {
        InitializeAudioSource();
        InitializeSoundsMainGame();
        InitializeClips();
        DontDestroyOnLoad(gameObject);
        PlayMainSound();
    }

    public static void PlayMainSound()
    {
        s_musicMainSource.PlayDelayed(1f);
    }

    public static void PlayMusicRewardflowSound()
    {
        s_musicRewardflowSource.PlayDelayed(0.5f);
    }

    public static void StopMainSound()
    {
        s_musicMainSource.DOFade(0, 1f).OnComplete(() => 
        { 
            s_musicMainSource.Stop();
            s_musicMainSource.volume = 1; 
        });
    }

    public static void StopMusicRewardflowSound()
    {
        s_musicRewardflowSource.DOFade(0,1f).OnComplete(() => 
        { 
            s_musicRewardflowSource.Stop(); 
            s_musicRewardflowSource.volume = 1; 
        });
    }

    public void InitializeSoundsMainGame()
    {
        s_musicMainSound = _musicMainSound;
        s_musicRewardflowSound = _musicRewardflowSound;

    }

    private void InitializeAudioSource()
    {
        s_musicMainSource = _musicMainSource;
        s_musicRewardflowSource = _musicRewardflowSource;
    }

    public void InitializationSoundsNewYearsEvent()
    {
        s_musicMainSound = _eventMusicMainSound;
        s_musicRewardflowSound = _eventMusicRewardflowSound;

    }

    public void InitializeClips()
    {
        s_musicMainSource.clip = s_musicMainSound;
        s_musicRewardflowSource.clip = s_musicRewardflowSound;
    }


} 
