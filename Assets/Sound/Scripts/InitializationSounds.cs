using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitializationSounds : MonoBehaviour
{
    [SerializeField] private UIAudioManager _uiAudioManager;
    [SerializeField] private ChestOpeningAudioManager _chestOpeningManager;
    [SerializeField] private EffectsAudioManager _effectsAudioManager;
    [SerializeField] private GoblinAudioManager _goblinAudioManager;
    [SerializeField] private MusicAudioManager _musicAudioManager;

    private bool _isMainGame;

    private void Start()
    {
        _isMainGame = true;
    }

    public void ChangingSounds()
    {
        
        if (_isMainGame)
        {
            InitializationSoundsNewYearsEvent();
            MusicAudioManager.PlayMainSound();
            _isMainGame = false;
        }
        else
        {
            InitializationSoundsMainGame();
            MusicAudioManager.PlayMainSound();
            _isMainGame = true;

        }
    }

    private void InitializationSoundsMainGame()
    {
        _uiAudioManager.InitializeSoundsMainGame();
        _chestOpeningManager.InitializeSoundsMainGame();
        _effectsAudioManager.InitializeSoundsMainGame();
        _goblinAudioManager.InitializeSoundsMainGame();
        _musicAudioManager.InitializeSoundsMainGame();
        _musicAudioManager.InitializeClips();
    }

    private void InitializationSoundsNewYearsEvent()
    {
        _uiAudioManager.InitializationSoundsNewYearsEvent();
        _chestOpeningManager.InitializationSoundsNewYearsEvent();
        _effectsAudioManager.InitializationSoundsNewYearsEvent();
        _goblinAudioManager.InitializationSoundsNewYearsEvent();
        _musicAudioManager.InitializationSoundsNewYearsEvent();
        _musicAudioManager.InitializeClips();
    }
}
