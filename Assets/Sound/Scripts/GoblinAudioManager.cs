using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoblinAudioManager : MonoBehaviour
{
    public static GoblinAudioManager instance = null;

    [Header("Main Game Sound")]
    [SerializeField] private AudioSource _goblinHammerChiselSource;
    [SerializeField] private AudioSource _goblinPickaxeSource;
    [SerializeField] private AudioSource _goblinHammerSource;
    [SerializeField] private AudioSource _goblinHandDrillSource;
    [SerializeField] private AudioSource _goblinJackhammerSource;
    [SerializeField] private AudioSource _goblinMegaDrillSource;
    [SerializeField] private AudioSource _goblinCircularSawSource;

    [SerializeField] private AudioSource _goblinMergeTargetSource;
    [SerializeField] private AudioSource _goblinMergeSuccessSource;

    [SerializeField] private AudioSource _goblinVoiceSource;

    [SerializeField] private AudioClip[] _goblinHammerChiselSound;
    [SerializeField] private AudioClip[] _goblinPickaxeSound;
    [SerializeField] private AudioClip[] _goblinHammerSound;
    [SerializeField] private AudioClip _goblinHandDrillSound;
    [SerializeField] private AudioClip _goblinJackhammerSound;
    [SerializeField] private AudioClip _goblinMegaDrillSound;
    [SerializeField] private AudioClip _goblinCircularSawSound;

    [SerializeField] private AudioClip _goblinMergeTargetSound;
    [SerializeField] private AudioClip _goblinMergeSuccessSound;

    [SerializeField] private AudioClip[] _goblinVoiceSoundSpawn;
    [SerializeField] private AudioClip[] _goblinVoiceSoundDragging;
    
    [Header("Main New Years Event Sound")]
    [SerializeField] private AudioClip[] _eventGoblinHammerChiselSound;
    [SerializeField] private AudioClip[] _eventGoblinPickaxeSound;
    [SerializeField] private AudioClip[] _eventGoblinHammerSound;

    [SerializeField] private AudioClip _eventGoblinMergeTargetSound;
    [SerializeField] private AudioClip _eventGoblinMergeSuccessSound;

    [SerializeField] private AudioClip[] _eventGoblinVoiceSoundSpawn;
    [SerializeField] private AudioClip[] _eventGoblinVoiceSoundDragging;


    [Header("Static")]
    static private AudioSource s_goblinHammerChiselSource;
    static private AudioSource s_goblinPickaxeSource;
    static private AudioSource s_goblinHammerSource;
    static private AudioSource s_goblinHandDrillSource;
    static private AudioSource s_goblinJackhammerSource;
    static private AudioSource s_goblinMegaDrillSource;
    static private AudioSource s_goblinCircularSawSource;
    static private AudioSource s_goblinMergeTargetSource;
    static private AudioSource s_goblinMergeSuccessSource;
    static private AudioSource s_goblinVoiceSource;

    static private AudioClip[] s_goblinHammerChiselSound;
    static private AudioClip[] s_goblinPickaxeSound;
    static private AudioClip[] s_goblinHammerSound;
    static private AudioClip s_goblinHandDrillSound;
    static private AudioClip s_goblinJackhammerSound;
    static private AudioClip s_goblinMegaDrillSound;
    static private AudioClip s_goblinCircularSawSound;
    static private AudioClip s_goblinMergeTargetSound;
    static private AudioClip s_goblinMergeSuccessSound;

    static private AudioClip[] s_goblinVoiceSoundSpawn;
    static private AudioClip[] s_goblinVoiceSoundDragging;

    private static int s_indexSoundHammerChisel;
    private static int s_indexSoundPickaxe;
    private static int s_indexSoundHammer;
    private static int s_randomIndexGoblinVoice;


    private void Start()
    {
        s_indexSoundHammerChisel = 0;
        s_indexSoundPickaxe = 0;
        s_indexSoundHammer = 0;

        InitializeAudioSource();
        InitializeSoundsMainGame();
        DontDestroyOnLoad(gameObject);
    }

    public static void PlaySoundGoblinHammerChisel()
    {
        PlayMultipleSounds(s_goblinHammerChiselSource, s_goblinHammerChiselSound, ref s_indexSoundHammerChisel);
    }

    public static void PlaySoundGoblinPickaxe()
    {
        PlayMultipleSounds(s_goblinPickaxeSource, s_goblinPickaxeSound, ref s_indexSoundPickaxe);
    }

    public static void PlaySoundGoblinHammer()
    {
        PlayMultipleSounds(s_goblinHammerSource, s_goblinHammerSound, ref s_indexSoundHammer);
    }

    public static void PlaySoundHandDrill()
    {
        s_goblinHandDrillSource.PlayOneShot(s_goblinHandDrillSound);
    }

    public static void PlaySoundJackhammer()
    {
        s_goblinJackhammerSource.PlayOneShot(s_goblinJackhammerSound);
    }

    public static void PlaySoundMegaDrill()
    {
        s_goblinMegaDrillSource.PlayOneShot(s_goblinMegaDrillSound);
    }
    public static void PlaySoundCircularSaw()
    {
        s_goblinCircularSawSource.PlayOneShot(s_goblinCircularSawSound);
    }

    public static void PlaySoundMergeTarget()
    {
        s_goblinMergeTargetSource.PlayOneShot(s_goblinMergeTargetSound);
    }

    public static void StopSoundMergeTarget()
    {
        s_goblinMergeTargetSource.Stop();
    }

    public static void PlaySoundMergeSuccess()
    {
        s_goblinMergeSuccessSource.PlayOneShot(s_goblinMergeSuccessSound);
    }



    private static void PlayMultipleSounds(AudioSource AudioSourceSound, AudioClip[] AudioClipSound, ref int IndexSound)
    {
        AudioSourceSound.PlayOneShot(AudioClipSound[IndexSound]);
        IndexSound++;
        if (IndexSound == AudioClipSound.Length)
            IndexSound = 0;
    }

    public static void PlayVoiceSoudSpawn()
    {
        s_goblinVoiceSource.Stop();
        s_randomIndexGoblinVoice = Random.Range(0, s_goblinVoiceSoundSpawn.Length);
        s_goblinVoiceSource.PlayOneShot(s_goblinVoiceSoundSpawn[s_randomIndexGoblinVoice]);
    }
    public static void PlayVoiceSoudSpawnDragging()
    {
        s_goblinVoiceSource.Stop();
        s_randomIndexGoblinVoice = Random.Range(0, s_goblinVoiceSoundDragging.Length);
        s_goblinVoiceSource.PlayOneShot(s_goblinVoiceSoundDragging[s_randomIndexGoblinVoice]);
    }

    private void InitializeAudioSource()
    {
        s_goblinHammerChiselSource = _goblinHammerChiselSource;
        s_goblinPickaxeSource = _goblinPickaxeSource;
        s_goblinHammerSource = _goblinHammerSource;
        s_goblinHandDrillSource = _goblinHandDrillSource;
        s_goblinJackhammerSource = _goblinJackhammerSource;
        s_goblinMegaDrillSource = _goblinMegaDrillSource;
        s_goblinCircularSawSource = _goblinCircularSawSource;
        s_goblinMergeTargetSource = _goblinMergeTargetSource;
        s_goblinMergeSuccessSource = _goblinMergeSuccessSource;
        s_goblinVoiceSource = _goblinVoiceSource;
    }


    public void InitializeSoundsMainGame()
    {
        s_goblinHammerChiselSound = _goblinHammerChiselSound;
        s_goblinPickaxeSound = _goblinPickaxeSound;
        s_goblinHammerSound = _goblinHammerSound;
        s_goblinHandDrillSound = _goblinHandDrillSound;
        s_goblinJackhammerSound = _goblinJackhammerSound;
        s_goblinMegaDrillSound = _goblinMegaDrillSound;
        s_goblinCircularSawSound = _goblinCircularSawSound;
        s_goblinMergeTargetSound = _goblinMergeTargetSound;
        s_goblinMergeSuccessSound = _goblinMergeSuccessSound;
        s_goblinVoiceSoundSpawn = _goblinVoiceSoundSpawn;
        s_goblinVoiceSoundDragging = _goblinVoiceSoundDragging;
    }

    public void InitializationSoundsNewYearsEvent()
    {
        s_goblinHammerChiselSound = _eventGoblinHammerChiselSound;
        s_goblinPickaxeSound = _eventGoblinPickaxeSound;
        s_goblinHammerSound = _eventGoblinHammerSound;
        s_goblinMergeTargetSound = _eventGoblinMergeTargetSound;
        s_goblinMergeSuccessSound = _eventGoblinMergeSuccessSound;
        s_goblinVoiceSoundSpawn = _eventGoblinVoiceSoundSpawn;
        s_goblinVoiceSoundDragging = _eventGoblinVoiceSoundDragging;
    }

}
