using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsTR1 : StateMachineBehaviour
{
    private float _hitTime;


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _hitTime = 0;
    }

    
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _hitTime += Time.deltaTime;
        
        if (_hitTime >= 1.1)
        {
            GoblinAudioManager.PlaySoundGoblinHammerChisel();
            _hitTime = 0;
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _hitTime = 0;
    }

}
